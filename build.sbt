// Global Values //

lazy val defaultScalaVersion = "2.12.4-bin-typelevel-4"
lazy val twitters = "Twitters"

// Common Settings
lazy val commonSettings = Seq(
  organization := "io.isomarcte",
  version := "1.0.0-SNAPSHOT",
  scalacOptions ++= Seq(
    // Taken from https://tpolecat.github.io/2017/04/25/scalac-flags.html
    "-deprecation",                     // Emit warning and location for usages of deprecated APIs.
    "-encoding", "utf-8",               // Specify character encoding used by source files.
    "-explaintypes",                    // Explain type errors in more detail.
    "-feature",                         // Emit warning and location for usages of features that should be imported explicitly.
    "-language:existentials",           // Existential types (besides wildcard types) can be written and inferred
    "-language:experimental.macros",    // Allow macro definition (besides implementation and application)
    "-language:higherKinds",            // Allow higher-kinded types
    "-language:implicitConversions",    // Allow definition of implicit functions called views
    "-unchecked",                       // Enable additional warnings where generated code depends on assumptions.
    "-Xcheckinit",                      // Wrap field accessors to throw an exception on uninitialized access.
    "-Xfuture",                         // Turn on future language features.
    "-Xlint:adapted-args",              // Warn if an argument list is modified to match the receiver.
    "-Xlint:by-name-right-associative", // By-name parameter of right associative operator.
    "-Xlint:constant",                  // Evaluation of a constant arithmetic expression results in an error.
    "-Xlint:delayedinit-select",        // Selecting member of DelayedInit.
    "-Xlint:doc-detached",              // A Scaladoc comment appears to be detached from its element.
    "-Xlint:inaccessible",              // Warn about inaccessible types in method signatures.
    "-Xlint:infer-any",                 // Warn when a type argument is inferred to be `Any`.
    "-Xlint:missing-interpolator",      // A string literal appears to be missing an interpolator id.
    "-Xlint:nullary-override",          // Warn when non-nullary `def f()' overrides nullary `def f'.
    "-Xlint:nullary-unit",              // Warn when nullary methods return Unit.
    "-Xlint:option-implicit",           // Option.apply used implicit view.
    "-Xlint:package-object-classes",    // Class or object defined in package object.
    "-Xlint:poly-implicit-overload",    // Parameterized overloaded implicit methods are not visible as view bounds.
    "-Xlint:private-shadow",            // A private field (or class parameter) shadows a superclass field.
    "-Xlint:stars-align",               // Pattern sequence wildcard must align with sequence component.
    "-Xlint:type-parameter-shadow",     // A local type parameter shadows a type already in scope.
    "-Xlint:unsound-match",             // Pattern match may not be typesafe.
    "-Yno-adapted-args",                // Do not adapt an argument list (either by inserting () or creating a tuple) to match the receiver.
    "-Ypartial-unification",            // Enable partial unification in type constructor inference
    "-Ywarn-extra-implicit",            // Warn when more than one implicit parameter section is defined.
    "-Ywarn-inaccessible",              // Warn about inaccessible types in method signatures.
    "-Ywarn-infer-any",                 // Warn when a type argument is inferred to be `Any`.
    "-Ywarn-nullary-override",          // Warn when non-nullary `def f()' overrides nullary `def f'.
    "-Ywarn-nullary-unit",              // Warn when nullary methods return Unit.
    "-Ywarn-numeric-widen",             // Warn when numerics are widened.
    "-Ywarn-unused:implicits",          // Warn if an implicit parameter is unused.
    "-Ywarn-unused:imports",            // Warn if an import selector is not referenced.
    "-Ywarn-unused:locals",             // Warn if a local definition is unused.
    "-Ywarn-unused:params",             // Warn if a value parameter is unused.
    "-Ywarn-unused:patvars",            // Warn if a variable bound in a pattern is unused.
    "-Ywarn-unused:privates",           // Warn if a private member is unused.
    "-Ywarn-value-discard",             // Warn when non-Unit expression results are unused.
    "-Yinduction-heuristics",           // speeds up the compilation of inductive implicit resolution
    "-Ykind-polymorphism",              // type and method definitions with type parameters of arbitrary kinds
    "-Yliteral-types",                  // literals can appear in type position
    "-Xstrict-patmat-analysis",         // more accurate reporting of failures of match exhaustivity
    "-Xlint:strict-unsealed-patmat"     // warn on inexhaustive matches against unsealed traits
  )
)

// Typelevel Compiler Settings //

inThisBuild(Seq(
  scalaOrganization := typelevelGroupId,
  scalaVersion := defaultScalaVersion
))

// Group Ids //

lazy val circeGroupId               = "io.circe"
lazy val commonsCodecGroupId        = "commons-codec"
lazy val fortysevendegGroupId       = "com.47deg"
lazy val fs2GroupId                 = "co.fs2"
lazy val http4sGroupId              = "org.http4s"
lazy val monocleGroupId             = "com.github.julien-truffaut"
lazy val pureConfigGroupId          = "com.github.pureconfig"
lazy val scalacheckGroupId          = "org.scalacheck"
lazy val scalacheckShapelessGroupId = "com.github.alexarchambault"
lazy val scalamacrosGroupId         = "org.scalamacros"
lazy val scodecGroupId              = "org.scodec"
lazy val scoptGroupId               = "com.github.scopt"
lazy val shapelessGroupId           = "com.chuusai"
lazy val specs2GroupId              = "org.specs2"
lazy val typelevelGroupId           = "org.typelevel"

// Artifact Ids //

lazy val catsCoreArtifactId                  = "cats-core"
lazy val catsEffectArtifactId                = "cats-effect"
lazy val circeCoreArtifactId                 = "circe-core"
lazy val circeGenericArtifactId              = "circe-generic"
lazy val circeJava8ArtifactId                = "circe-java8"
lazy val circeParserArtifactId               = "circe-parser"
lazy val circeShapesArtifactId               = "circe-shapes"
lazy val circeTestingArtifactId              = "circe-testing"
lazy val commonsCodecArtifactId              = commonsCodecGroupId
lazy val disciplineArtifactId                = "discipline"
lazy val fs2CatsArtifactId                   = "fs2-cats"
lazy val fs2CoreArtifactId                   = "fs2-core"
lazy val fs2IoArtifactId                     = "fs2-io"
lazy val http4sBlazeClientArtifactId         = "http4s-blaze-client"
lazy val http4sCoreArtifactId                = "http4s-core"
lazy val http4sTestingArtifactId             = "http4s-testing"
lazy val monocleArtifactId                   = "monocle-core"
lazy val monocleMacroArtifactId              = "monocle-macro"
lazy val paradiseArtifactId                  = "paradise"
lazy val pureConfigArtifactId                = "pureconfig"
lazy val scalacheckArtifactId                = "scalacheck"
lazy val scalacheckShapelessArtifactId       = "scalacheck-shapeless_1.13"
lazy val scalacheckToolboxDateTimeArtifactId = "scalacheck-toolbox-datetime"
lazy val scodecBitsArtifactId                = "scodec-bits"
lazy val scodecCoreArtifactId                = "scodec-core"
lazy val scodecStreamArtifactId              = "scodec-stream"
lazy val scoptArtifactId                     = "scopt"
lazy val shapelessArtifactId                 = "shapeless"
lazy val specs2CoreArtifactId                = "specs2-core"
lazy val specs2ScalacheckArtifactId          = "specs2-scalacheck"
lazy val spireArtifactId                     = "spire"

// Versions //

lazy val catsEffectVersion          = "0.3"
lazy val catsVersion                = "0.9.0"
lazy val circeVersion               = "0.8.0"
lazy val commonsCodecVersion        = "1.11"
lazy val disciplineVersion          = "0.8"
lazy val fs2CatsVersion             = "0.3.0"
lazy val fs2Version                 = "0.9.7"
lazy val http4sVersion              = "0.17.6"
lazy val monocleVersion             = "1.4.0"
lazy val paradiseVersion            = "2.1.0"
lazy val pureConfigVersion          = "0.8.0"
lazy val scalacheckShapelessVersion = "1.1.7"
lazy val scalacheckToolboxVersion   = "0.2.3"
lazy val scalacheckVersion          = "1.13.5"
lazy val scodecBitsVersion          = "1.1.5"
lazy val scodecCoreVersion          = "1.10.3"
lazy val scodecStreamVersion        = "1.0.1"
lazy val scoptVersion               = "3.7.0"
lazy val shapelessVersion           = "2.3.2"
lazy val specs2Version              = "4.0.2"
lazy val spireVersion               = "0.14.1"

// Dependencies //

lazy val catsCore                  = typelevelGroupId %% catsCoreArtifactId % catsVersion
lazy val catsEffect                = typelevelGroupId %% catsEffectArtifactId % catsEffectVersion
lazy val circeCore                 = circeGroupId %% circeCoreArtifactId % circeVersion
lazy val circeGeneric              = circeGroupId %% circeGenericArtifactId % circeVersion
lazy val circeJava8                = circeGroupId %% circeJava8ArtifactId % circeVersion
lazy val circeParser               = circeGroupId %% circeParserArtifactId % circeVersion
lazy val circeShapes               = circeGroupId %% circeShapesArtifactId % circeVersion
lazy val circeTesting              = circeGroupId %% circeTestingArtifactId % circeVersion
lazy val commonsCodec              = commonsCodecGroupId % commonsCodecArtifactId % commonsCodecVersion
lazy val discipline                = typelevelGroupId %% disciplineArtifactId % disciplineVersion
lazy val fs2Cats                   = fs2GroupId %% fs2CatsArtifactId % fs2CatsVersion
lazy val fs2Core                   = fs2GroupId %% fs2CoreArtifactId % fs2Version
lazy val fs2Io                     = fs2GroupId %% fs2IoArtifactId % fs2Version
lazy val http4sBlazeClient         = http4sGroupId %% http4sBlazeClientArtifactId % http4sVersion
lazy val http4sCore                = http4sGroupId %% http4sCoreArtifactId % http4sVersion
lazy val http4sTesting             = http4sGroupId %% http4sTestingArtifactId % http4sVersion
lazy val monocle                   = monocleGroupId %% monocleArtifactId % monocleVersion
lazy val monocleMacro              = monocleGroupId %% monocleMacroArtifactId % monocleVersion
lazy val paradise                  = scalamacrosGroupId %% paradiseArtifactId % paradiseVersion
lazy val pureConfig                = pureConfigGroupId %% pureConfigArtifactId % pureConfigVersion
lazy val scalacheck                = scalacheckGroupId %% scalacheckArtifactId % scalacheckVersion
lazy val scalacheckShapeless       = scalacheckShapelessGroupId %% scalacheckShapelessArtifactId % scalacheckShapelessVersion
lazy val scalacheckToolboxDateTime = fortysevendegGroupId %% scalacheckToolboxDateTimeArtifactId % scalacheckToolboxVersion
lazy val scodecBits                = scodecGroupId %% scodecBitsArtifactId % scodecBitsVersion
lazy val scodecCore                = scodecGroupId %% scodecCoreArtifactId % scodecCoreVersion
lazy val scodecStream              = scodecGroupId %% scodecStreamArtifactId % scodecStreamVersion
lazy val scopt                     = scoptGroupId %% scoptArtifactId % scoptVersion
lazy val shapeless                 = shapelessGroupId %% shapelessArtifactId % shapelessVersion
lazy val specs2Core                = specs2GroupId %% specs2CoreArtifactId % specs2Version
lazy val specs2Scalacheck          = specs2GroupId %% specs2ScalacheckArtifactId % specs2Version
lazy val spire                     = typelevelGroupId %% spireArtifactId % spireVersion

// Root Project //

lazy val root =
  (project in file(".")).settings(
    commonSettings,
    name := s"$twitters"
  ).aggregate(
    twittersOAuth,
    twittersCirceStream,
    twittersModel,
    twittersHttpClient,
    twittersCli,
    twittersAnalytics
  )

// Modules //

lazy val twittersOAuth =
  (project in file("twitters-oauth")).settings(
    commonSettings,
    name := s"${twitters}-oauth",
    libraryDependencies ++= Seq(
      catsCore,
      fs2Cats,
      fs2Core,
      scalacheck % Test,
      commonsCodec % Test,
      specs2Core % Test,
      specs2Scalacheck % Test
    )
  )

lazy val twittersCirceStream =
  (project in file("twitters-circe-stream")).settings(
    commonSettings,
    name := s"${twitters}-circe-stream",
    libraryDependencies ++= Seq(
      catsCore,
      circeCore,
      circeParser,
      fs2Core,
      scodecBits,
      spire,
      circeTesting % Test,
      fs2Io % Test,
      scalacheck % Test,
      scalacheckShapeless % Test,
      specs2Core % Test,
      specs2Scalacheck % Test
    )
  )

lazy val twittersModel =
  (project in file("twitters-model")).settings(
    commonSettings,
    name := s"${twitters}-model",
    addCompilerPlugin(paradise cross CrossVersion.patch),
    libraryDependencies ++= Seq(
      catsCore,
      spire,
      monocle % Optional,
      monocleMacro % Optional,
      circeCore % Optional,
      circeGeneric % Optional,
      circeJava8 % Optional,
      circeTesting % Test,
      circeShapes % Test,
      discipline % Test,
      fs2Cats % Test,
      fs2Core % Test,
      fs2Io % Test,
      scalacheck % Test,
      scalacheckShapeless % Test,
      scalacheckToolboxDateTime % Test,
      shapeless % Test,
      specs2Core % Test,
      specs2Scalacheck % Test
    )
  ).dependsOn(
    twittersCirceStream % "test->test"
  )

lazy val twittersHttpClient =
  (project in file("twitters-http-client")).settings(
    commonSettings,
    name := s"${twitters}-http-client",
    libraryDependencies ++= Seq(
      monocle,
      http4sBlazeClient,
      http4sTesting % Test,
      scalacheck % Test,
      scalacheckShapeless % Test,
      specs2Core % Test,
      specs2Scalacheck % Test
    )
  ).dependsOn(
    twittersOAuth % "compile->compile",
    twittersOAuth % "test->test",
    twittersCirceStream
  )

lazy val twittersAnalytics =
  (project in file("twitters-analytics")).settings(
    commonSettings,
    name := s"${twitters}-analytics",
    addCompilerPlugin(paradise cross CrossVersion.patch),
    libraryDependencies ++= Seq(
      catsCore,
      circeCore,
      circeGeneric,
      circeParser,
      fs2Core,
      spire,
      monocle,
      monocleMacro,
      scalacheck % Test,
      scalacheckShapeless % Test,
      specs2Core % Test,
      specs2Scalacheck % Test
    )
  ).dependsOn(
    twittersModel
  )

lazy val twittersCli =
  (project in file("twitters-cli")).settings(
    commonSettings,
    name := s"${twitters}-cli",
    assemblyJarName in assembly := "twitters-cli.jar",
    libraryDependencies ++= Seq(
      catsCore,
      fs2Core,
      fs2Cats,
      circeGeneric,
      circeShapes,
      pureConfig,
      scopt,
      shapeless,
      http4sBlazeClient,
      scalacheck % Test,
      scalacheckShapeless % Test,
      specs2Core % Test,
      specs2Scalacheck % Test
    )
  ).dependsOn(
    twittersOAuth,
    twittersModel,
    twittersCirceStream,
    twittersHttpClient,
    twittersAnalytics
  )
