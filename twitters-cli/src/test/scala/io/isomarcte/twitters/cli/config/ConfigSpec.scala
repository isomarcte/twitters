package io.isomarcte.twitters.cli.config

// Java Standard Library Imports //

import java.nio.file.Paths

// General Imports //

import fs2.Task

import org.specs2.Specification

// Module Imports //

import io.isomarcte.twitters.oauth._

final class ConfigSpec extends Specification {

  // Private Values //

  private[this] lazy val unitTest0 = {
    val expectedConsumerKey: ConsumerKey =
      ConsumerKey("baz")
    val expectedConsumerSecret: ConsumerSecret =
      ConsumerSecret("boof")
    val expectedOAuthToken: OAuthToken =
      OAuthToken("derp")
    val expectedOAuthSecret: OAuthTokenSecret =
      OAuthTokenSecret("foobar")
    val expectedOAuthSecrets: OAuthSecrets =
      OAuthSecrets(
        expectedOAuthSecret,
        expectedConsumerSecret
      )

    (for {
      result <- Config.fromPath[Task](Paths.get(this.getClass.getResource("/ExampleConfig.json").toURI))
    } yield result.oauthConfig match {
      case OAuthConfig(
        _,
        consumerKey,
        oauthToken,
        oauthSecrets
      ) => (consumerKey must_== expectedConsumerKey) and
           (oauthToken must_== expectedOAuthToken) and
           (oauthSecrets must_== expectedOAuthSecrets)
    }).unsafeRun
  }

  // Specification //

  def is = s2"""

  Config Specification


  Reading the example config from a file should yield the correct result. ${this.unitTest0}
  """

}
