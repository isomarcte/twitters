package io.isomarcte.twitters.cli

// Java Standard Library Imports //

import java.nio.charset.StandardCharsets.UTF_8

// General Imports //

import cats.implicits._

import io.circe._
import io.circe.shapes._

import fs2.Task
import fs2.Stream
import fs2.Strategy
import fs2.async.mutable._

import fs2.interop.cats._

import org.http4s._
import org.http4s.client.blaze._

// Module Imports //

import io.isomarcte.twitters.model._

import io.isomarcte.twitters.http.client._

import io.isomarcte.twitters.circe.stream.CirceStream

import io.isomarcte.twitters.analytics._

// Local Imports //

import io.isomarcte.twitters.cli.config._

/** The Main class for the command line interface. */
object Main {

  // Private Functions //

  private[this] def responseGuard(
    r: Response
  ): Stream[Task, Response] =
    if (r.status.isSuccess) {
      Stream(r)
    } else {
      Stream.fail(new IllegalStateException(s"Unexpected response $r"))
    }

  private[this] def selectTweet(
    t: Types.TwitterJson
  ): Stream[Task, Tweet] =
    t.select[Tweet].map(t => Stream(t)).getOrElse(
      t.select[Json].map(j =>
        Stream.eval(
          Task.now(System.err.println(s"Unable to decode JSON $j"))
        ) >> Stream.empty
      ).getOrElse(Stream.empty))

  /** Asynchronously process a http4s Response of [[Tweet]] values into analytics and log
    * them to the console.
    *
    * @param r the Response to process.
    *
    * @return a Stream which yields Unit. Results are logged to the console.
    */
  private[this] def processResponseAsync(
    r: Response
  ): Stream[Task, Unit] = {
    val decoder: Decoder[Types.TwitterJson] = Decoder[Types.TwitterJson]
    val tweetDecoder: Decoder[Tweet] = Decoder[Tweet]
    val threads: Int =
      scala.math.max(Runtime.getRuntime.availableProcessors(), 4)
    implicit val strat: Strategy =
      Strategy.fromFixedDaemonPool(threads)
    val queue: Task[Queue[Task, Json]] =
      Queue.bounded(1024)

    (for {
      q <- Stream.eval(queue)
      enqueue = q.enqueue(CirceStream.jsonByteFrame(UTF_8)(r.body))
      dequeue = Analytics.pipeline[Task](for {
        j <- q.dequeue
        result <- decoder.decodeJson(j).bimap(
          err =>
          Stream.eval(Task.now(println(tweetDecoder.decodeJson(j)))) >>
            Stream.eval(Task.now(s"Invalid Json: $j")) >>
            Stream.fail(new IllegalStateException(s"ERR: $j \n $err")),
          value => this.selectTweet(value)
        ).merge
      } yield result)
      result <- enqueue.drain.mergeHaltL(dequeue)
    } yield result).evalMap(a => Analytics.toResultString[Task](a).map(println))
  }

  /** Process a http4s Response of [[Tweet]] values into analytics and log
    * them to the console.
    *
    * @note in my experiments this was well able to keep up with reading
    *       tweets even at rates of 60+ tweets/second.
    *
    * @param r the Response to process.
    *
    * @return a Stream which yields Unit. Results are logged to the console.
    */
  private[this] def processResponse(
    r: Response
  ): Stream[Task, Unit] = {
    val decoder: Decoder[Types.TwitterJson] = Decoder[Types.TwitterJson]
    val tweetDecoder: Decoder[Tweet] = Decoder[Tweet]
    for {
      analytics <- Analytics.pipeline[Task](for {
        j <- CirceStream.jsonByteFrame(UTF_8)(r.body)
        result <- decoder.decodeJson(j).bimap(
          err =>
          Stream.eval(Task.now(println(tweetDecoder.decodeJson(j)))) >>
            Stream.eval(Task.now(s"Invalid Json: $j")) >>
            Stream.fail(new IllegalStateException(s"ERR: $j \n $err")),
          value => value.select[Tweet].map(v => Stream(v)).getOrElse(Stream.empty)
        ).merge
      } yield result)
      result <- Stream.eval(Analytics.toResultString[Task](analytics).map(println))
    } yield result
  }

  // Public Functions //

  /** The main entry point for the command line program. */
  def main(args: Array[String]): Unit =
    CliConfig.parser.parse(args, CliConfig()) match {
      case Some(cliConfig) =>
        (for {
          config <- Config.fromPath[Task](cliConfig.oauthConfigPath)
          client <- Task.delay(PooledHttp1Client())
          request <- Requests.authorizeRequest[Task](config.oauthConfig)(Requests.sampleRealtimeTweets)
          result <- client.streaming(request)(r =>
            for {
              r <- this.responseGuard(r)
              result <- this.processResponseAsync(r)
            } yield result).run
        } yield result).attempt.flatMap{
          case Left(t) =>
            Task.now(System.err.println(t.getLocalizedMessage))
          case _ => Task.now(())
        }.unsafeRun
      case None => ()
    }

}
