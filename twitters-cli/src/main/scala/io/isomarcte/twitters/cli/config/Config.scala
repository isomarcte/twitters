package io.isomarcte.twitters.cli.config

// Java Standard Library Imports //

import java.nio.file.Path

// General Imports //

import cats.implicits._

import fs2.util.Effect
import fs2.util.syntax.FunctorOps

import pureconfig._

// Module Imports //

import io.isomarcte.twitters.oauth._

/** The configuration for the program.
  *
  * @constructor
  * @param oauthConfig the [[OAuthConfig]] for connecting to Twitter.
  */
final case class Config(
  oauthConfig: OAuthConfig
)

/** Utilities for working with [[Config]]. */
object Config {

  // Private Records //

  /** An internal record type for modeling the JSON configuration file.
    */
  private[this] final case class FileConfig(
    oauthToken: String,
    oauthSecret: String,
    consumerKey: String,
    consumerSecret: String
  )

  // Public Values //

  /** Given a Path to the configuration, yield the configuration.
    *
    * @param path a Path to the configuration.
    *
    * @tparam E an effect type.
    *
    * @return an effect which will yield the configuration when run.
    */
  def fromPath[E[_] : Effect](
    path: Path
  ): E[Config] = {
    val effectInstance: Effect[E] = Effect[E]
    effectInstance.suspend(
      loadConfig[FileConfig](path).bimap(
        failures =>
        effectInstance.fail[Config](new IllegalStateException(failures.toString)),
        {
          case FileConfig(
            oauthToken,
            oauthSecret,
            consumerKey,
            consumerSecret
          ) =>
            OAuthConfig.defaultSecureRandom(
              ConsumerKey(consumerKey),
              OAuthToken(oauthToken),
              OAuthSecrets(
                OAuthTokenSecret(oauthSecret),
                ConsumerSecret(consumerSecret)
              )).map(o => Config(o))
        }
      ).merge
    )
  }
}
