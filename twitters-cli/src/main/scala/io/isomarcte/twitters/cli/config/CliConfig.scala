package io.isomarcte.twitters.cli.config

// Java Standard Library Imports

import java.io.File
import java.nio.file.Path
import java.nio.file.Paths

/** Record for the command line parameters.
  *
  * @param oauthConfigPath the Path to the OAuth configuration file.
  */
final case class CliConfig(
  oauthConfigPath: Path = Paths.get(".", ".twitters")
)

/** Utilities for working with [[CliConfig]].*/
object CliConfig {

  /** The parser for the command line options. */
  val parser = new scopt.OptionParser[CliConfig]("twitters"){
    head("twitters-cli")
    opt[File]('c', "config").optional().valueName("OAuth Config").action(
      (v, c) => c.copy(oauthConfigPath = v.toPath)
    )
  }

}
