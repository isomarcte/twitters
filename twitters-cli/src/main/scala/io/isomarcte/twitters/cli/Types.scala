package io.isomarcte.twitters.cli

// General Imports //

import io.circe.Json

import shapeless._

// Module Imports //

import io.isomarcte.twitters.model._

/** Type aliases. */
object Types {

  /** A Shapeless CoProduct type for all of the values which we expect to read
    * from the Twitter stream.
    */
  type TwitterJson =
    Tweet :+:
    DeleteNotice :+:
    Warning :+:
    StatusWithheldNotice :+:
    UserWithheldNotice :+:
    Json :+:
    CNil
}
