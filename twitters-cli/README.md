# Twitters Cli #

This module provides a command line interface to the twitters application.

# Setup #

To create a runnable jar for twitters we will use [sbt-assembly][assembly].

```shell
$ sbt
> project twittersCli
> assembly
```

Note, some of the dependent modules do a large amount of compile time typeclass generation. As such, if you are doing a clean compilation you may need to give sbt more heap space, e.g. `sbt -mem 8000`.

This will yield a runnable `jar` file called `twitters-cli.jar` in the `twitters-cli/target/scala-2.12/` folder.

# Configuration And Execution #

The `twitters-cli.jar` requires some configuration to run. Since this is being run _locally_ we require the full set of OAuth parameters to connect to Twitter. That is, the OAuth Token, OAuth Secret, Consumer Key, and Consumer Secret. Generation of the Consumer Secret will require you to register an application with twitter, http://apps.twitter.com/ .

These should be placed in a JSON file with the following format.

```json
{
    "oauth-token": "derp",
    "oauth-secret": "derp",
    "consumer-key": "derp",
    "consumer-secret": "derp"
}
```

If not provided, `twitters-cli.jar` will look for a file `.twitters` in the working directory. An explicit file can be provided with the `-c/--config` option. For example,

```shell
$ java -jar twitters-cli.jar -c ~/.twitters
```

# Running #

Once `twitters-cli.jar` is running it will continuously output to the console with update analytics from Twitter. For example,

```
Twitter Analtyics (Started at 2017-12-30T22:54:48.219Z)
 Tweets Total                                 : 3889
 Duration Reading Stream                      : PT1M57.529S
 Tweets/Second                                : 33.23931623931623931623931623931624
 Tweets/Minute                                : 1994.358974358974358974358974358974
 Tweets/Hour                                  : 119661.5384615384615384615384615384
 Percentage of Tweets with Emoji              : 17.51092825919259449730007714065312%
 Percentage of Tweets with URLs               : 23.21933659038313191051684237593212%
 Percentage of Tweets with Photo Domain Links : 0.6428387760349704294163023913602469%
 Top 5 Domains                                : (twitter.com,422), (bit.ly,54), (du3a.org,48), (youtu.be,40), (fb.me,33)
 Top 5 HashTags                               : (FollowMeCarter,17), (VideoMTV2017,16), (İbrahimÇelikkol,6), (زد_رصيدك89,5), (CareerArc,5)
 Top 5 Emoji                                  : (Emoji(FACE WITH TEARS OF JOY,Set(NonEmptyList(128514))),233), (Emoji(HEAVY BLACK HEART,Set(NonEmptyList(10084, 65039), NonEmptyList(10084))),64), (Emoji(LOUDLY CRYING FACE,Set(NonEmptyList(128557))),61), (Emoji(TWO HEARTS,Set(NonEmptyList(128149))),55), (Emoji(SMILING FACE WITH HEART-SHAPED EYES,Set(NonEmptyList(128525))),43)
```

[assembly]: https://github.com/sbt/sbt-assembly "sbt-assembly"
