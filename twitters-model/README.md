# Twitters Model #

This module contains ADTs for modeling JSON from twitter.

## Non-Idiomatic Naming ##

Many of the ADT fields break with standard Scala camelCase naming conventions. This is because, as much as was possible, the record keys were made to line up _exactly_ with the JSON keys. This makes deriving typeclass instances much more simple.

## A Note On Compilation Times ##

This module derives a large number of typeclass instances (through shapeless). This can lead to longer than normal compilation times. On my system this ends up being, at most, 3 minutes, and often much less.

## A Note On Tests ##

Tests were written for most of the non-derived codecs and the top level codecs. Non-top level records with derived codecs are validated through the top-level tests.
