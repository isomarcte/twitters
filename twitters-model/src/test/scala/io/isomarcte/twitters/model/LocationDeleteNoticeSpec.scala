package io.isomarcte.twitters.model

// General Imports //

import io.circe.testing.CodecTests

import org.scalacheck.ScalacheckShapeless._

import org.specs2.Specification

import org.typelevel.discipline.specs2.Discipline

/** Tests that the LocationDelete value encodes/decodes properly. */
final class LocationDeleteNoticeSpec extends Specification with Discipline {
  // Specifications //

  def is = s2"""

  LocationDeleteNotice Data Model Specification

  ${this.checkAll("Codec[LocationDeleteNotice]", CodecTests[LocationDeleteNotice].codec)}
  """
}
