package io.isomarcte.twitters.model

// General Imports //

import com.fortysevendeg.scalacheck.datetime.jdk8.ArbitraryJdk8._

import io.circe.testing.CodecTests

import org.scalacheck.ScalacheckShapeless._

import org.specs2.Specification

import org.typelevel.discipline.specs2.Discipline

/** Tests that the Tweet record encodes/decodes properly. */
final class TweetSpec extends Specification with Discipline {

  // Specifications //

  def is = s2"""

  Tweet Data Model Specification

  ${this.checkAll("Codec[Tweet]", CodecTests[Tweet].codec)}
  """
}
