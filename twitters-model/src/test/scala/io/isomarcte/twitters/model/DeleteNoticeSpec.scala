package io.isomarcte.twitters.model

// General Imports //

import io.circe.testing.CodecTests

import org.scalacheck.ScalacheckShapeless._

import org.specs2.Specification

import org.typelevel.discipline.specs2.Discipline

/** Tests that the DeleteNotice value encodes/decodes properly. */
final class DeleteNoticeSpec extends Specification with Discipline {
  // Specifications //

  def is = s2"""

  DeleteNotice Data Model Specification

  ${this.checkAll("Codec[DeleteNotice]", CodecTests[DeleteNotice].codec)}
  """
}
