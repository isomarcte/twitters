package io.isomarcte.twitters.model

// General Imports //

import io.circe._
import io.circe.syntax._

import io.circe.testing.CodecTests

import org.scalacheck.ScalacheckShapeless._

import org.specs2.Specification

import org.typelevel.discipline.specs2.Discipline

/** Tests that the Coordinates record encodes/decodes properly. */
final class CoordinatesSpec extends Specification with Discipline {

  // Private Values //

  // Unit Test

  /** A Unit test for a known Coordinates value. */
  private[this] val unitTest0 = {
    val coordinates: Json = Json.arr(Json.fromInt(1), Json.fromInt(2))
    Decoder[Coordinates].decodeJson(coordinates).map(_.asJson) must_== Right(coordinates)
  }

  // Specifications //

  def is = s2"""

  Coordinate Data Model Specification

  ${this.checkAll("Codec[Coordinate]", CodecTests[Coordinates].codec)}

  A Coordinate should be isomorphic with a length two Json array. ${this.unitTest0}
  """
}
