package io.isomarcte.twitters.model

// General Imports //

import io.circe.testing.CodecTests

import org.scalacheck.ScalacheckShapeless._

import org.specs2.Specification

import org.typelevel.discipline.specs2.Discipline

/** Tests that the WithheldContentNotice encodes/decodes properly. */
final class UserWithheldNoticeSpec extends Specification with Discipline {
  // Specifications //

  def is = s2"""

  WithheldContentNotice Data Model Specification

  ${this.checkAll("Codec[UserWithheldNotice]", CodecTests[UserWithheldNotice].codec)}
  """
}
