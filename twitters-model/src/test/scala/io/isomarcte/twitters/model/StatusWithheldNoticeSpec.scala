package io.isomarcte.twitters.model

// General Imports //

import io.circe.testing.CodecTests

import org.scalacheck.ScalacheckShapeless._

import org.specs2.Specification

import org.typelevel.discipline.specs2.Discipline

/** Tests that the WithheldContentNotice encodes/decodes properly. */
final class StatusWithheldNoticeSpec extends Specification with Discipline {
  // Specifications //

  def is = s2"""

  WithheldContentNotice Data Model Specification

  ${this.checkAll("Codec[StatusWithheldNotice]", CodecTests[StatusWithheldNotice].codec)}
  """
}
