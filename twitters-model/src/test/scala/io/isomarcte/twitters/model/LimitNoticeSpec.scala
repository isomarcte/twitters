package io.isomarcte.twitters.model

// General Imports //

import io.circe.testing.CodecTests

import org.scalacheck.ScalacheckShapeless._

import org.specs2.Specification

import org.typelevel.discipline.specs2.Discipline

/** Tests that the LimitNotice value encodes/decodes properly. */
final class LimitNoticeSpec extends Specification with Discipline {
  // Specifications //

  def is = s2"""

  LimitNotice Data Model Specification

  ${this.checkAll("Codec[LimitNotice]", CodecTests[LimitNotice].codec)}
  """
}
