package io.isomarcte.twitters.model

// Java Standard Library Imports //

import java.nio.charset.StandardCharsets.UTF_8

// General Imports //

import cats.implicits._

import fs2.Stream
import fs2.Task
import fs2.util.Effect
import fs2.util.Suspendable

import io.circe._
import io.circe.shapes._

import org.specs2.Specification

import shapeless._

// Module Imports //

import io.isomarcte.twitters.circe.stream.CirceStream

import io.isomarcte.twitters.circe.stream.util.StreamTestData

/** Tests for decoding streams of Coproducts of model values. */
final class StreamDecodeSpec extends Specification {

  // Private Types //

  /** A Coproduct of either a Tweet or a DeleteNotice. */
  private[this] type TweetStream = Tweet :+: DeleteNotice :+: CNil

  // Private Values //

  /** A Decoder for a TweetStream. */
  private[this] val tweetStreamDecoder: Decoder[TweetStream] =
    Decoder[TweetStream]

  // Unit Tests

  /** A test for decoding a stream which contains both Tweet values and
    * DeleteNotice values.
    */
  private[this] val tweetStreamUnit0 =
    this.tweetStream[Task].map((s: Stream[Task, TweetStream]) =>
      s.run.map(_ => true must_== true).unsafeRun
    )

  // Private Functions //

  /** Creates a Stream of Json values from the test resources. */
  private[this] def jsonStream[E[_] : Suspendable]: List[Stream[E, Json]] =
    StreamTestData.tweetResourcePaths.toList.map(p =>
      CirceStream.jsonByteFrame(UTF_8)(
      StreamTestData.streamTestData[E](512, p)
      )
    )

  /** Creates a Stream of TweetStream values from the test resources, failing
    * the Stream unless ''all'' values decode properly.
    */
  private[this] def tweetStream[E[_] : Effect]: List[Stream[E, TweetStream]] =
    this.jsonStream.map((s: Stream[E, Json]) =>
      s.flatMap{((j: Json) =>
        tweetStreamDecoder.decodeJson(j).bimap(
          err => Stream.fail(err),
          tweet => Stream(tweet)
        ).merge
      )})


  // Specification //

  def is = s2"""

  Stream Decode of Json Model Values Specification


  Stream decodeing a coproduct of values should not yield errors. ${this.tweetStreamUnit0}
  """
}
