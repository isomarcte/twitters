package io.isomarcte.twitters.model

// General Imports //

import io.circe._

import io.circe.parser._

import io.circe.testing.CodecTests

import org.scalacheck.ScalacheckShapeless._

import org.specs2.Specification

import org.typelevel.discipline.specs2.Discipline

/** Tests that the WithheldContentNotice encodes/decodes properly. */
final class WarningSpec extends Specification with Discipline {

  // Private Values //

  private[this] final val warningJson: String =
    """
    {
      "warning" : {
        "code" : "FALLING_BEHIND",
        "message" : "Your connection is falling behind and messages are being queued for delivery to you. Your queue is now over 30% full. You will be disconnected when the queue is full.",
        "percent_full" : 30,
        "timestamp_ms" : "1513724885876"
      }
    }"""

  // Unit Tests

  private[this] final val unitTest0 = {
    val d: Decoder[Warning] = Decoder[Warning]
    val message: String =
      "Your connection is falling behind and messages are being queued for delivery to you. Your queue is now over 30% full. You will be disconnected when the queue is full."

    (for {
      j <- parse(this.warningJson)
      w <- d.decodeJson(j)
    } yield w) must_== Right(Warning(
      StallWarning(
        "FALLING_BEHIND",
        message,
        30.0,
        "1513724885876")))
  }

  // Specifications //

  def is = s2"""

  WithheldContentNotice Data Model Specification

  ${this.checkAll("Codec[Warning]", CodecTests[Warning].codec)}

  Decoding a known value will yield the correct result. ${this.unitTest0}
  """
}
