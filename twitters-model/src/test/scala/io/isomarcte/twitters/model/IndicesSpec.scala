package io.isomarcte.twitters.model

// General Imports //

import io.circe._
import io.circe.syntax._

import io.circe.testing.CodecTests

import org.scalacheck.ScalacheckShapeless._

import org.specs2.Specification

import org.typelevel.discipline.specs2.Discipline

/** Tests that the Indices record encodes/decodes properly. */
final class IndicesSpec extends Specification with Discipline {

  // Private Values //

  // Unit Test

  /** A Unit test for a known Indices value. */
  private[this] val unitTest0 = {
    val indices: Json = Json.arr(Json.fromInt(1), Json.fromInt(2))
    Decoder[Indices].decodeJson(indices).map(_.asJson) must_== Right(indices)
  }

  // Specifications //

  def is = s2"""

  Indices Data Model Specification

  ${this.checkAll("Codec[Indices]", CodecTests[Indices].codec)}

  The Indices [1, 2] should decode and encode symmetrically. ${unitTest0}
  """
}
