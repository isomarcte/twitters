package io.isomarcte.twitters.model.scalacheck

// Java Standard Library Imports //

import java.net.URL

// General Imports //

import org.scalacheck.Arbitrary
import org.scalacheck.Gen

/** Orphan Arbitrary Instances.
  *
  * @note since they are orphan instances, they should not be exported.
  */
private[twitters] trait OrphanArbitraryInstances {

  // Typeclass Instances //

  implicit final val arbUrl: Arbitrary[URL] =
    Arbitrary(Gen.oneOf(
      new URL("http://twitter.com"),
      new URL("https://twitter.com")))
}

/** Concrete instance of [[OrphanArbitraryInstances]]. */
private[twitters] object OrphanArbitraryInstances extends OrphanArbitraryInstances
