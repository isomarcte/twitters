package io.isomarcte.twitters.model

// General Imports //

import cats.kernel

import io.circe.generic.JsonCodec

@JsonCodec final case class StatusWithheldNotice(
  status_withheld: StatusWithheld
)

trait StatusWithheldNoticeInstances {

  // Typeclass Instances //

  implicit final val statusWithheldNoticeEq: kernel.Eq[StatusWithheldNotice] =
    kernel.Eq.fromUniversalEquals
}

object StatusWithheldNotice extends StatusWithheldNoticeInstances
