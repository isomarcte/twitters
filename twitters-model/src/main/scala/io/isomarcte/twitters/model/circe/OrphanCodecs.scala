package io.isomarcte.twitters.model.circe

// Java Standard Library Imports //

import java.net.URL
import java.time.ZoneId

// Scala Standard Library Imports //

import scala.util.Try

// General Imports //

import io.circe._

import spire.math.UInt
import spire.math.ULong
import spire.math.UShort

/** Orphan Circe Codecs. */
private[twitters] trait OrphanCodecs {

  implicit val ushortEncoder: Encoder[UShort] =
    Encoder[Int].contramap(_.toInt)

  implicit val ushortDecoder: Decoder[UShort] =
    Decoder[Int].emap{
      case i if i <= UShort.MaxValue.toInt =>
        Right(UShort(i))
      case i =>
        Left(s"JSON value $i is greater than expected max ${UShort.MaxValue}")
    }

  implicit val ulongEncoder: Encoder[ULong] =
    Encoder[BigInt].contramap(_.toBigInt)

  implicit val ulongDecoder: Decoder[ULong] =
    Decoder[BigInt].emap{
      case bi if bi <= ULong.MaxValue.toBigInt =>
        Right(ULong.fromBigInt(bi))
      case bi =>
        Left(s"JSON value $bi is greater than expected max ${ULong.MaxValue}")
    }

  implicit val uintEncoder: Encoder[UInt] =
    Encoder[Long].contramap(_.toLong)

  implicit val uintDecoder: Decoder[UInt] =
    Decoder[Long].emap{
      case l if l <= UInt.MaxValue.toLong =>
        Right(UInt(l))
      case l =>
        Left(s"JSON value $l is greater than expected max ${UInt.MaxValue}")
    }

  implicit val urlEncoder: Encoder[URL] =
    Encoder[String].contramap(_.toString)

  implicit val urlDecoder: Decoder[URL] =
    Decoder[String].emapTry((s: String) =>
      Try(new URL(s))
    )

  // Remove once https://github.com/circe/circe/pull/813 is merged

  implicit val decodeZoneId: Decoder[ZoneId] =
    Decoder[String].emap{
      case s if ZoneId.getAvailableZoneIds.contains(s) =>
        Right(ZoneId.of(s))
      case s =>
        Left(s"$s is not an recognized ZoneId.")
    }

  implicit val encodeZoneId: Encoder[ZoneId] =
    Encoder[String].contramap(_.toString)
}

/** Concrete instances of the orphan circe codecs. */
private[twitters] object OrphanCodecs extends OrphanCodecs
