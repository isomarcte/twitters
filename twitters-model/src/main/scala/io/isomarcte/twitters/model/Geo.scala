package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

@JsonCodec final case class Geo(
  `type`: String,
  coordinates: Coordinates
)
