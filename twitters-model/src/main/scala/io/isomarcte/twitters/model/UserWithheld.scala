package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

import spire.math.ULong

// Local Imports //

import io.isomarcte.twitters.model.circe.OrphanCodecs._

@JsonCodec final case class UserWithheld(
  id: ULong,
  withheld_in_countries: List[String],
  timestamp_ms: String
)
