package io.isomarcte.twitters.model

// General Imports //

import cats._

import io.circe._

import spire.math.UShort

// Local Imports //

import io.isomarcte.twitters.model.circe.OrphanCodecs._

/** A newtype for indicies.
  *
  * @note the twitter JSON represents indicies values as a JSON array which
  *       universally contains exactly two elements. Since such a
  *       representation is inherently unsafe, the model here is to represent
  *       it as a standard product.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object]]
  */
final case class Indices(
  indices: (UShort, UShort)
) extends AnyVal

/** Typeclass instances for [[Indices]]
  *
  * @note encoder and decoder instances can not be derived because of the
  *       differences in the Scala and JSON representation for this type, so
  *       they must be created manually.
  */
trait IndicesInstances {

  // Typeclass Instances //

  /** An [[Encoder]] instance for [[Indices]]. */
  implicit final val indiciesEncoder: Encoder[Indices] =
    Encoder[List[UShort]].contramap{
      case Indices((a, b)) => List(a, b)
    }

  /** A [[Decoer]] instance for [[Indices]]. */
  implicit final val indiciesDecoder: Decoder[Indices] =
    Decoder[List[UShort]].emap{
      case a :: b :: Nil => Right(Indices((a, b)))
      case l =>
        Left(
          s"An indicies array must contain exactly two values. Found ${l.size}"
        )
    }
  /** An [[Eq]] instance for [[Indices]]. */
  implicit val indiciesEq: kernel.Eq[Indices] =
    Eq.fromUniversalEquals
}

/** Typeclass instances, such that the compiler can resolve them. */
object Indices extends IndicesInstances
