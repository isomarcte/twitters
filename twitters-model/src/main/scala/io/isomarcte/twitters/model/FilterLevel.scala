package io.isomarcte.twitters.model

// General Imports //

import io.circe._

/** An ADT for a filter level.
  *
  * @note since filter levels can only take on three possible values, encoded
  *       as JSON strings, this is effectively an enumeration.
  */
sealed trait FilterLevel extends Product with Serializable

/** Typeclass instances for [[FilterLevel]].
  *
  * @note Since we wish to explicitly model the closed nature of the possible
  *       values for this ADT, the codecs for it must be manually created.
  */
trait FilterLevelInstances {

  // Private Values //

  // JSON Key values
  private[this] final val NoneString: "none" = "none"
  private[this] final val LowString: "low" = "low"
  private[this] final val MediumString: "medium" = "medium"

  // Typeclass Instances //

  /** An Encoder instance for [[FilterLevel]]. */
  implicit final lazy val filterLevelEncoder: Encoder[FilterLevel] =
    Encoder[String].contramap{
      case FilterLevel.FilterLevelNone => this.NoneString
      case FilterLevel.FilterLevelLow => this.LowString
      case FilterLevel.FilterLevelMedium => this.MediumString
    }

  /** A Decoder instance for [[FilterLevel]]. */
  implicit final lazy val filterLevelDecoder: Decoder[FilterLevel] =
    Decoder[String].emap{
      case NoneString => Right(FilterLevel.FilterLevelNone)
      case LowString => Right(FilterLevel.FilterLevelLow)
      case MediumString => Right(FilterLevel.FilterLevelMedium)
      case err => Left(s"$err is not a valid filter level.")
    }
}

/** ADT Constructors and Typeclass instances for [[FilterLevel]]. */
object FilterLevel extends FilterLevelInstances {

  final case object FilterLevelNone extends FilterLevel
  final case object FilterLevelLow extends FilterLevel
  final case object FilterLevelMedium extends FilterLevel
}
