package io.isomarcte.twitters.model

// General Imports //

import cats._
import cats.kernel.Eq
import cats.implicits._

import io.circe._
// import io.circe.syntax._

import monocle.Prism

/** Record type for a Coordinates pair.
  *
  * @note the twitter JSON represents coordinates values as a JSON array which
  *       universally contains exactly two elements. Since such a
  *       representation is inherently unsafe, the model here is to represent
  *       it as a standard product.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/geo-objects#place]]
  */
final case class Coordinates(
  longitude: Double,
  latitude: Double
)

/** Typeclass Instances for [[Coordinates]]
  *
  * Since the structure of the Scala representation and the JSON
  * representation is different, a codec can not be derived for this type and
  * must be manually created.
  */
trait CoordinatesInstances {

  // Optics //

  /** A Prism between a List of Double values and a [[Coordinates]] value. */
  val coordinatesPrism: Prism[List[Double], Coordinates] =
    Prism[List[Double], Coordinates]{
      case longitude :: latitude :: Nil => Coordinates(longitude, latitude).pure[Option]
      case _ => None
    }{
      case Coordinates(longitude, latitude) => List(longitude, latitude)
    }

  // Typeclass Instances //

  /** An Encoder instance for [[Coordinates]]. */
  implicit val coordinatesEncoder: Encoder[Coordinates] =
    Encoder[List[Double]].contramap(this.coordinatesPrism.reverseGet(_))

  /** An Decoder instance for [[Coordinates]]. */
  implicit val coordinatesDecoder: Decoder[Coordinates] =
    Decoder[List[Double]].emap((l: List[Double]) =>
      this.coordinatesPrism.getOrModify(
        l
      ).toEither.leftMap((l: List[Double]) =>
        s"JSON Array has invalid size to be a Coordinates value. Size is ${l.size}"
      )
    )

  /** An Eq instance for [[Coordinates]]. */
  implicit val coordinatesEqInstance: Eq[Coordinates] =
    Eq.fromUniversalEquals

  /** An Show instance for [[Coordinates]]. */
  implicit val coordinatesShowInstance: Show[Coordinates] =
    Show.fromToString
}

/** Typeclass instance for [[Coordinate]]. */
object Coordinates extends CoordinatesInstances
