package io.isomarcte.twitters.model

// Generic Imports //

import io.circe._
import io.circe.syntax._

/** An ADT for a resize object.
  *
  * @note since filter levels can only take on three possible values, encoded
  *       as JSON strings, this is effectively an enumeration.
  */
sealed trait Resize extends Product with Serializable

/** Typeclass instances for [[Resize]].
  *
  * @note Since we wish to explicitly model the closed nature of the possible
  *       values for this ADT, the codecs for it must be manually created.
  */
trait ResizeInstances {

  // Private Values //

  // JSON Key Values
  private[this] final val FitString: "fit" = "fit"
  private[this] final val CropString: "crop" = "crop"

  // Typeclass Instances //

  /** An Encoder instance for [[Resize]]. */
  implicit final lazy val resizeEncoder: Encoder[Resize] =
    new Encoder[Resize] {
      override final def apply(r: Resize): Json =
        r match {
          case Resize.Fit => FitString.asJson
          case Resize.Crop => CropString.asJson
        }
    }

  /** A Decoder instance for [[Resize]]. */
  implicit final lazy val resizeDecoder: Decoder[Resize] =
    Decoder[String].emap{
      case FitString => Right(Resize.Fit)
      case CropString => Right(Resize.Crop)
      case err => Left(s"$err is not a valid resize")
    }
}

/** ADT Constructors and Typeclass instances for [[Resize]]. */
object Resize extends ResizeInstances {
  final case object Fit extends Resize
  final case object Crop extends Resize
}
