package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

/** A record type for symbol objects
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object]]
  */
@JsonCodec final case class Symbol(
  indices: Indices,
  text: String
)
