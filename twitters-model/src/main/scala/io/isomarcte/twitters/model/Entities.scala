package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

/** A record type for an entities object.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object]]
  */
@JsonCodec final case class Entities(
  hashtags: Option[Vector[Hashtag]],
  media: Option[Vector[Media]],
  urls: Option[Vector[TwitterUrl]],
  user_mentions: Option[Vector[UserMentions]],
  symbols: Option[Vector[Symbol]],
  polls: Option[Vector[Poll]]
)
