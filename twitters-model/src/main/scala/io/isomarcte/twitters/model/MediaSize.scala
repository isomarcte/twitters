package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

/** A record type for media size object.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object]]
  */
@JsonCodec final case class MediaSize(
  thumb: Size,
  large: Size,
  medium: Size,
  small: Size
)
