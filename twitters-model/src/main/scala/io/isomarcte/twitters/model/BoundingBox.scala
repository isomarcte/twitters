package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

/** Record type for a bounding box.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/geo-objects#place]]
  */
@JsonCodec final case class BoundingBox(
  `type`: String,
  coordinates: Vector[Vector[Coordinates]]
)
