package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

/** A record for a place object.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/geo-objects#place]]
  */
@JsonCodec final case class Place(
  id: String,
  url: String,
  place_type: String,
  name: String,
  full_name: String,

  // Replace with java.util.Locale.IsoCountryCode after Java 9 is lowest
  // supported version.
  country_code: String,
  country: String,
  bounding_box: Option[BoundingBox]
)
