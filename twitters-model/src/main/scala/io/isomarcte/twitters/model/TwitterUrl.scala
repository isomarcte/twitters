package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

/** A record type for twitter URL objects.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object]]
  */
@JsonCodec final case class TwitterUrl(
  // Warning: This is not a true URL. They are implying the scheme.
  display_url: String,
  expanded_url: String,
  indices: Indices,
  url: String
)
