package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

import spire.math.ULong

// Local Imports //

import io.isomarcte.twitters.model.circe.OrphanCodecs._

/** A record type for a delete status.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/filter-realtime/guides/streaming-message-types]]
  */
@JsonCodec final case class DeleteStatus(
  id: ULong,
  id_str: String,
  user_id: ULong,
  user_id_str: String
)
