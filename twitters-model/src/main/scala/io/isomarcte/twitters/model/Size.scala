package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

import spire.math.ULong

// Local Imports //

import io.isomarcte.twitters.model.circe.OrphanCodecs._

/** A record type for size objects.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object]]
  */
@JsonCodec final case class Size(
  w: ULong,
  h: ULong,
  resize: Resize
)
