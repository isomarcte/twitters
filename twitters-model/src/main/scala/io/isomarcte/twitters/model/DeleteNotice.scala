package io.isomarcte.twitters.model

// General Imports //

import cats.kernel

import io.circe._
import io.circe.syntax._

/** Record type for a delete notice.
  *
  * @note the JSON representation contains a superfluous wrapper object which
  *       is not represented in the Scala model.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/filter-realtime/guides/streaming-message-types]]
  */
final case class DeleteNotice(
  status: DeleteStatus,
  timestamp_ms: String
)

/** Typeclass instances for [[DeleteNotice]].
  *
  * @note since the JSON representation contains an extra wrapper object, the
  *       codecs for this type can not be derived and must be manually created.
  */
trait DeleteNoticeInstances {

  // Private Values //

  // JSON Key values
  private[this] final val deleteString: "delete" = "delete"
  private[this] final val statusString: "status" = "status"
  private[this] final val timestamp_msString: "timestamp_ms" = "timestamp_ms"

  // Typeclass Instances //

  /** An Encoder for a [[DeleteNotice]]. */
  implicit final val deleteNoticeEndocer: Encoder[DeleteNotice] =
    new Encoder[DeleteNotice]{
      override final def apply(d: DeleteNotice): Json =
        Json.obj(
          deleteString -> Json.obj(
            statusString -> d.status.asJson,
            timestamp_msString -> d.timestamp_ms.asJson
          ))
    }

  /** An Decoder for a [[DeleteNotice]]. */
  implicit final val deleteNoticeDecoder: Decoder[DeleteNotice] =
    new Decoder[DeleteNotice]{
      override final def apply(c: HCursor): Decoder.Result[DeleteNotice] = {
        val unwrapped: ACursor = c.downField(deleteString)
        for {
          status <- unwrapped.downField(statusString).as[DeleteStatus]
          timestamp_ms <- unwrapped.downField(timestamp_msString).as[String]
        } yield DeleteNotice(status, timestamp_ms)
      }
    }

  /** An Eq instance for a [[DeleteNotice]]. */
  implicit final val deleteEq: kernel.Eq[DeleteNotice] =
    kernel.Eq.fromUniversalEquals
}

/** Typeclass instances for [[DeleteNotice]]. */
object DeleteNotice extends DeleteNoticeInstances
