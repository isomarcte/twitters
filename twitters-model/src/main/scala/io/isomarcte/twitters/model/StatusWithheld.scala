package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

import spire.math.ULong

// Local Imports //

import io.isomarcte.twitters.model.circe.OrphanCodecs._

@JsonCodec final case class StatusWithheld(
  id: ULong,
  user_id: ULong,
  withheld_in_countries: List[String],
  timestamp_ms: String
)
