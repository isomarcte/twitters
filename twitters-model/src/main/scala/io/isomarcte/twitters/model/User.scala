package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

import spire.math.ULong

// Local Imports //

import io.isomarcte.twitters.model.circe.OrphanCodecs._

/** A record type for a twitter user object.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object.html]]
  */
@JsonCodec final case class User(
  id: ULong,
  id_str: String,
  name: String,
  screen_name: String,
  location: Option[String],
  url: Option[String],
  description: Option[String],
  derived: Option[String],
  `protected`: Boolean,
  verified: Boolean,
  followers_count: ULong,
  friends_count: ULong,
  listed_count: ULong,
  favourites_count: ULong,
  statuses_count: ULong,
  created_at: String,
  utc_offset: Option[Int],
  time_zone: Option[String],
  geo_enabled: Boolean,

  // Replace with java.util.Locale.IsoCountryCode after Java 9 is lowest
  // supported version.
  lang: String,
  contributors_enabled: Boolean,
  profile_background_color: String,
  profile_background_image_url: String,
  profile_background_image_url_https: String,
  profile_background_tile: Boolean,
  profile_banner_url: Option[String],
  profile_image_url: String,
  profile_image_url_https: String,
  profile_link_color: String,
  profile_sidebar_border_color: String,
  profile_sidebar_fill_color: String,
  profile_text_color: String,
  profile_use_background_image: Boolean,
  default_profile: Boolean,
  default_profile_image: Boolean,

  // Replace with java.util.Locale.IsoCountryCode after Java 9 is lowest
  // supported version.
  withheld_in_countries: Option[String],
  withheld_scope: Option[String]
)
