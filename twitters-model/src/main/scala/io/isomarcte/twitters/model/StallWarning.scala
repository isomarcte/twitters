package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

@JsonCodec final case class StallWarning(
  code: String,
  message: String,
  percent_full: Double,
  timestamp_ms: String
)
