package io.isomarcte.twitters.model

// General Imports //

import cats.kernel

import io.circe.generic.JsonCodec

@JsonCodec final case class UserWithheldNotice(
  status_withheld: UserWithheld
)

trait UserWithheldNoticeInstances {

  implicit final val userWithheldNoticeEq: kernel.Eq[UserWithheldNotice] =
    kernel.Eq.fromUniversalEquals
}

object UserWithheldNotice extends UserWithheldNoticeInstances
