package io.isomarcte.twitters.model

// General Imports //

import cats._

import io.circe._
import io.circe.syntax._

import spire.math.ULong

// Local Imports //

import io.isomarcte.twitters.model.circe.OrphanCodecs._

/** A record for a LimitNotice.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/filter-realtime/guides/streaming-message-types]]
  */
final case class LimitNotice(
  track: ULong
)

/** Typeclass instances for [[LimitNotice]]
  *
  * @note the twitter JSON creates a superfluous wrapper object for this
  *       [[LimitNotice]] so the encoder/decoder instances can not be derived
  *       and must be encoded manually.
  */
trait LimitNoticeInstances {

  // Private Values //

  // JSON Key values
  private[this] final val limit_String: "limit" = "limit"
  private[this] final val track_String: "track" = "track"

  // Typeclass Instances //

  /** An Encoder for [[LimitNotice]]. */
  implicit final val limitNoticeEncoder: Encoder[LimitNotice] =
    new Encoder[LimitNotice]{
      override final def apply(l: LimitNotice): Json =
        Json.obj(limit_String ->
          Json.obj(
            track_String -> l.track.asJson
          )
        )
    }

  /** A Decoder for [[LimitNotice]]. */
  implicit final val limitNoticeDecoder: Decoder[LimitNotice] =
    new Decoder[LimitNotice] {
      override final def apply(c: HCursor): Decoder.Result[LimitNotice] = {
        val unwraped: ACursor = c.downField(limit_String)
        for {
          track <- unwraped.downField(track_String).as[ULong]
        } yield LimitNotice(track)
      }
    }

  /** An Eq for [[LimitNotice]]. */
  implicit final val limitNoticeEq: kernel.Eq[LimitNotice] =
    kernel.Eq.fromUniversalEquals
}

/** Typeclass instances, such the the compiler may resolve them. */
object LimitNotice extends LimitNoticeInstances
