package io.isomarcte.twitters.model

// General Imports //

import cats._

import io.circe._
import io.circe.syntax._

import spire.math.ULong

// Local Imports //

import io.isomarcte.twitters.model.circe.OrphanCodecs._

/** A record type for a Location Delete Notice.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/filter-realtime/guides/streaming-message-types]]
  */
final case class LocationDeleteNotice(
  user_id: ULong,
  user_id_str: String,
  up_to_status_id: ULong,
  up_to_status_id_str: String
)

/** Typeclass instances for [[LocationDeleteNotice]].
  *
  * @note the twitter JSON encodes a [[LocationDeleteNotice]] in a superfluous
  *       wrapper object, and as such the encoder/decoder instances can not be
  *       derived but must be encoded manually.
  */
trait LocationDeleteNoticeInstances {

  // Private Values //

  // JSON Key Values
  private[this] final val scrub_geo_String: "scrub_geo" = "scrub_geo"
  private[this] final val user_id_String: "user_id" = "user_id"
  private[this] final val user_id_str_String: "user_id_str" = "user_id_str"
  private[this] final val up_to_status_id_String: "up_to_status_id" = "up_to_status_id"
  private[this] final val up_to_status_id_str_String: "up_to_status_id_str" = "up_to_status_id_str"

  // Typeclass Instances //

  /** An Encoder instance for [[LocationDeleteNotice]]. */
  implicit final val locationDeleteNoticeEncoder: Encoder[LocationDeleteNotice] =
    new Encoder[LocationDeleteNotice]{
      override final def apply(l: LocationDeleteNotice): Json =
        Json.obj(scrub_geo_String ->
          Json.obj(
            user_id_String -> l.user_id.asJson,
            user_id_str_String -> l.user_id_str.asJson,
            up_to_status_id_String -> l.up_to_status_id.asJson,
            up_to_status_id_str_String -> l.up_to_status_id_str.asJson,
          )
        )
    }

  /** An Decoder instance for [[LocationDeleteNotice]]. */
  implicit final val locationDeleteNoticeDecoder: Decoder[LocationDeleteNotice] =
    new Decoder[LocationDeleteNotice] {
      override final def apply(c: HCursor): Decoder.Result[LocationDeleteNotice] = {
        val unwraped: ACursor = c.downField(scrub_geo_String)
        for {
          user_id <- unwraped.downField(user_id_String).as[ULong]
          user_id_str <- unwraped.downField(user_id_str_String).as[String]
          up_to_status_id <- unwraped.downField(up_to_status_id_String).as[ULong]
          up_to_status_id_str <- unwraped.downField(up_to_status_id_str_String).as[String]
        } yield LocationDeleteNotice(user_id, user_id_str, up_to_status_id, up_to_status_id_str)
      }
    }

  /** An Eq instance for [[LocationDeleteNotice]]. */
  implicit final val locationDeleteNoticeEq: kernel.Eq[LocationDeleteNotice] =
    kernel.Eq.fromUniversalEquals
}

/** Typeclass instances, such that the compiler may resolve them. */
object LocationDeleteNotice extends LocationDeleteNoticeInstances
