package io.isomarcte.twitters.model

// General Imports //

import cats._

import io.circe.generic.JsonCodec

import spire.math.ULong

// Local Imports //

import io.isomarcte.twitters.model.circe.OrphanCodecs._

/** Record type for a Tweet.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object.html]]
  */
@JsonCodec final case class Tweet(
  created_at: String,
  id: ULong,
  id_str: String,
  text: String,
  source: String,
  truncated: Boolean,
  in_reply_to_status_id: Option[ULong],
  in_reply_to_status_id_str: Option[String],
  in_reply_to_user_id: Option[ULong],
  in_reply_to_user_id_str: Option[String],
  in_reply_to_screen_name: Option[String],
  user: User,
  coordinates: Option[Geo],
  place: Option[Place],
  quoted_status_id: Option[ULong],
  quoted_status_id_str: Option[String],
  is_quote_status: Boolean,
  quote_status: Option[Tweet],
  retweeted_status: Option[Tweet],
  quote_count: Option[ULong],
  reply_count: ULong,
  retweet_count: ULong,

  // They use the British spelling in User but the American spelling in Tweet?
  favorite_count: Option[ULong],
  entities: Entities,
  extended_entities: Option[ExtendedEntities],
  favorited: Option[Boolean],
  retweeted: Boolean,
  possibly_sensitive: Option[Boolean],
  filter_level: FilterLevel,

  // Replace with java.util.Locale.IsoCountryCode after Java 9 is lowest
  // supported version.
  lang: String
)

/** Typeclass Instances for [[Tweet]]. */
trait TweetInstances {

  // Typeclass Instances //

  implicit final val tweetEq: kernel.Eq[Tweet] =
    kernel.Eq.fromUniversalEquals
}

/** Typeclass Instances for [[Tweet]], so the compiler can resolve them. */
object Tweet extends TweetInstances
