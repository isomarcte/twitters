package io.isomarcte.twitters.model

// General Imports //

import cats.kernel

import io.circe.generic.JsonCodec

@JsonCodec final case class Warning(
  warning: StallWarning
) extends AnyVal

trait WarningInstances {

  implicit final val warningEqInstance: kernel.Eq[Warning] =
    kernel.Eq.fromUniversalEquals
}

object Warning extends WarningInstances
