package io.isomarcte.twitters.model

// Java Standard Library Imports //

import java.time.ZonedDateTime

// General Imports //

import io.circe.generic.JsonCodec

import io.circe.java8.time._

import spire.math.UInt

// Local Imports //

import io.isomarcte.twitters.model.circe.OrphanCodecs._

/** A record for a poll object.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object#polls]]
  */
@JsonCodec final case class Poll(
  options: Vector[TwitterOption],
  end_datetime: ZonedDateTime,

  // Type is wrong in the Twitter API doc.
  duration_minutes: UInt
)
