package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

import spire.math.UShort

// Local Imports //

import io.isomarcte.twitters.model.circe.OrphanCodecs._

/** A record type for twitter option values.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object]]
  */
@JsonCodec final case class TwitterOption(
  position: UShort,
  text: String
)
