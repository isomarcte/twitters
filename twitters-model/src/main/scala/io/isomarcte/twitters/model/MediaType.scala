package io.isomarcte.twitters.model

// General Imports //

import io.circe._
import io.circe.syntax._

/** An ADT for a media type.
  *
  * @note since media types can only take on three possible values, encoded as
  *       JSON strings, this is effectively an enumeration.
  */
sealed trait MediaType extends Product with Serializable

/** Typeclass instances for [[MediaType]].
  *
  * @note Since we wish to explicitly model the closed nature of the possible
  *       values for this ADT, the codecs for it must be manually created.
  */
trait MediaTypeInstances {

  // Private Values //

  // JSON Key Values
  private[this] final val PhotoString: "photo" = "photo"
  private[this] final val VideoString: "video" = "video"
  private[this] final val AnimatedGifString: "animated_gif" = "animated_gif"

  // Typeclass Instances //

  /** An Encoder for a [[MediaType]]. */
  implicit final lazy val mediaTypeEncoder: Encoder[MediaType] =
    new Encoder[MediaType]{
      override final def apply(m: MediaType): Json =
        m match {
          case MediaType.Photo => PhotoString.asJson
          case MediaType.Video => VideoString.asJson
          case MediaType.AnimatedGif => AnimatedGifString.asJson
        }
    }

  /** A Decoder for a [[MediaType]]. */
  implicit final lazy val mediaTypeDecoder: Decoder[MediaType] =
    Decoder[String].emap{
      case PhotoString => Right(MediaType.photo)
      case VideoString => Right(MediaType.video)
      case AnimatedGifString => Right(MediaType.animatedGif)
      case s => Left(s"Unknown MediaType $s")
    }
}

/** ADT Constructors, smart constructors, and Typeclass Instances for
  * [[MediaType]].
  */
object MediaType extends MediaTypeInstances {

  // ADT Constructors //

  final case object Photo extends MediaType
  final case object Video extends MediaType
  final case object AnimatedGif extends MediaType

  // Smart Constructors //

  val photo: MediaType = this.Photo
  val video: MediaType = this.Video
  val animatedGif: MediaType = this.AnimatedGif
}
