package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

import spire.math.ULong

// Local Imports //

import io.isomarcte.twitters.model.circe.OrphanCodecs._

/** A record type for a media object
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object]]
  */
@JsonCodec final case class Media(
  display_url: String,
  expanded_url: String,
  id: ULong,
  id_str: String,
  indices: Indices,
  media_url: String,
  media_url_https: String,
  sizes: MediaSize,
  score_status_id: Option[ULong],

  // They list this as an Int64 in the docs...
  score_status_id_str: Option[String],
  `type`: MediaType,
  url: String
)
