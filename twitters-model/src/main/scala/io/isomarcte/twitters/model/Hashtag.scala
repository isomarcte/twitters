package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

/** A record type for Hashtag.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object]]
  */
@JsonCodec final case class Hashtag(
  indices: Indices,
  text: String
)
