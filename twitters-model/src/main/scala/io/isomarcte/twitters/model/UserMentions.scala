package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

import spire.math.ULong

// Local Imports //

import io.isomarcte.twitters.model.circe.OrphanCodecs._

/** A record type for a twitter user mentions object.
  *
  * @see [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object]]
  */
@JsonCodec final case class UserMentions(
  id: Option[ULong],
  id_str: Option[String],
  indices: Indices,
  name: Option[String],
  screen_name: String
)
