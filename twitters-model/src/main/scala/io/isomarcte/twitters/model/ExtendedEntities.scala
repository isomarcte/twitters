package io.isomarcte.twitters.model

// General Imports //

import io.circe.generic.JsonCodec

/** A newtype for an extended entities object.
  *
  * @see
  * [[https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/extended-entities-object]]
  */
@JsonCodec final case class ExtendedEntities(
  media: Vector[Media]
) extends AnyVal
