# Twitters Circe FS2 Stream #

This module provides simple and non-generic utilities for reading streams of newline delimited Json values from Streams of Bytes.

The primary interface can be found in `CirceStream`, in particular `CirceStream#jsonByteFrame`.
