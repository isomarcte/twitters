package io.isomarcte.twitters.circe.stream

// General Imports //

import scodec.bits.ByteVector

/** An internal ADT used to model a traversal of a Stream of Bytes. */
private[circe] sealed trait StreamBuffer extends Product with Serializable

/** ADT Constructors and smart constructors. */
private[circe] object StreamBuffer {

  // ADT Constructors //

  /** A record for a buffer which has already been fully processed. Such a
    * record will not have a delimiter value in it.
    *
    * @constructor
    * @param bv the underlying ByteVector buffer.
    */
  private[circe] final case class ProcessedBuffer(
    bv: ByteVector
  ) extends StreamBuffer

  /** A record for a buffer which has not bee fully processed. Such a record
    * may have a delimiter in it and should be examined before pulling more
    * values.
    *
    * @constructor
    * @param bv the underlying ByteVector buffer.
    */
  private[circe] final case class NonProcessedBuffer(
    bv: ByteVector
  ) extends StreamBuffer

  // Public Functions

  // Smart Constructors

  /** Create an empty [[StreamBuffer]]. */
  private[circe] def empty: StreamBuffer =
    ProcessedBuffer(ByteVector.empty)
}
