package io.isomarcte.twitters.circe.stream

// Java Standard Library Imports //

import java.nio.charset.Charset

// Scala Standard Library Imports //

import scala.util.Try

// General Imports //

import cats.implicits._

import fs2.Handle
import fs2.NonEmptyChunk
import fs2.Pipe
import fs2.Pull
import fs2.Stream

import io.circe.Json

import io.circe.parser._

import scodec.bits.ByteVector

/** Functions for operating on Streams of newline delimited Json values.
  *
  * @note this targets the Twitter Streaming API and is unlikely to be
  *       generally useful.
  */
object CirceStream {

  // Private Values //

  /** Constant byte for a newline character. */
  private[this] val tenByte: Byte = 10.toByte

  /** Partitions a ByteVector into a span.
    *
    * @note this span function differs from those you might seen in the
    *       standard library in that consumes the Byte which matches the
    *       predicate. It also explicitly models the situation in which the
    *       entire ByteVector fails the predicate by using an Option in the
    *       second position of the pair.
    *
    * @param predicate a predicate at which to split the ByteVector into two
    *        component ByteVector values.
    *
    * @param byteVector the ByteVector to split.
    *
    * @return a pair of a ByteVector and an Option of a ByteVector. In the
    *         case when the second position is empty the entire ByteVector
    *         failed the predicate.
    */
  private[this] def spanByteVector(
    predicate: Byte => Boolean
  )(
    byteVector: ByteVector
  ): (ByteVector, Option[ByteVector]) =
    byteVector.toArray.span(predicate) match {
      case (h, t) if t.size === 0 =>
        (ByteVector.view(h), None)
      case (h, t) =>
        (ByteVector.view(h), Some(ByteVector.view(t.tail)))
    }

  /** A Pull used to partition a Stream of Bytes into a Stream of frames of
    * ByteVectors partitioned by one or more newline characters, e.g. 0xa.
    *
    * @note all newline delimiters are removed from the Stream.
    *
    * @param buffer a buffer which holds partially consumed bytes from the
    *        Stream.
    *
    * @param h a handle from which to pull more values from the Stream.
    *
    * @tparam E an * -> * type for the Stream, usually effectful.
    *
    * @return a Pull which will partition the Stream into newline delimited frames.
    */
  private[this] def newlinePull[E[_]](
    buffer: StreamBuffer
  )(
    h: Handle[E, Byte]
  ): Pull[E, ByteVector, Nothing] = {

    // Check the state of the buffer. If it contains not yet processed values,
    // that is values which may have a newline delimiter in them, process
    // these values before pulling anything else from the handle
    def checkBuffer(
      buffer: StreamBuffer,
      h: Handle[E, Byte]
    ): Pull[E, ByteVector, ((ByteVector, Option[ByteVector]), Handle[E, Byte])] = {
      val f: ByteVector => (ByteVector, Option[ByteVector]) =
        this.spanByteVector(_ =!= this.tenByte)(_)
      buffer match {
        case StreamBuffer.ProcessedBuffer(bv) =>
          for {
            opt <- h.awaitOption
            (nextChunk, h) <- flush(bv, opt)
          } yield (f(prepend(bv, nextChunk)), h)
        case StreamBuffer.NonProcessedBuffer(bv) =>
          Pull.pure((f(bv), h))
      }
    }

    // If the stream is exhausted, flush the buffer, if non-empty, and
    // terminate the Pull.
    def flush(
      buffer: ByteVector,
      opt: Option[(NonEmptyChunk[Byte], Handle[E, Byte])]
    ): Pull[E, ByteVector, (NonEmptyChunk[Byte], Handle[E, Byte])] =
      opt match {
        case Some((chunk, h)) => Pull.pure((chunk, h))
        case _ =>
          if (buffer.isEmpty) {
            Pull.done
          } else {
            Pull.output1(buffer) >> Pull.done
          }
      }

    // Prepend the buffer to the next chunk.
    def prepend(
      buffer: ByteVector,
      chunk: NonEmptyChunk[Byte]
    ): ByteVector =
      buffer ++ ByteVector.view(chunk.toArray)

    for {
      (spanResult, h) <- checkBuffer(buffer, h)
      step <- spanResult match {
        case (partial, None) =>
          this.newlinePull(StreamBuffer.ProcessedBuffer(partial))(h)
        case (full, Some(remainder)) if full.isEmpty =>
          // Twitter is sending keepalive newlines
          this.newlinePull(StreamBuffer.NonProcessedBuffer(remainder))(h)
        case (full, Some(remainder)) =>
          Pull.output1(full) >> this.newlinePull(StreamBuffer.NonProcessedBuffer(remainder))(h)
      }
    } yield step
  }

  // Private Functions //

  /** Given a Stream of Bytes, partition it into a Stream of ByteVector frames
    * which are delimited by newline characters.
    *
    * @tparam E an * -> * type for the Stream, usually effectful.
    *
    * @return a Pipe from a Stream of Bytes to a Stream of ByteVector frames.
    */
  def frame[E[_]]: Pipe[E, Byte, ByteVector] =
    _.pull(this.newlinePull[E](StreamBuffer.empty))

  /** Given a Stream of ByteVector frames, attempt to parse each frame as a
    * Json value.
    *
    * @param charset the Character set to use when decoding the Stream. Note
    *        that only UTF-8 and UTF-16 are considered valid by the RFC.
    *
    * @tparam E an * -> * type for the Stream, usually effectful.
    *
    * @return a Pipe from a Stream of Bytes to a Stream of Json values.
    */
  def jsonFrame[E[_]](
    charset: Charset
  ): Pipe[E, ByteVector, Json] =
    (s: Stream[E, ByteVector]) =>
      s.flatMap((bv: ByteVector) =>
        (for {
          str <- Try(new String(bv.toArray, charset)).toEither
          json <- parse(str)
        } yield json
        ) match {
          case Right(json) => Stream(json)
          case Left(err) => Stream.fail(err)
        }
      )

  /** Given a Stream of Bytes, treat it as newline delimited Json and attempt
    * to parse it into Json.
    *
    * @param charset the Character set to use when decoding the Stream. Note
    *        that only UTF-8 and UTF-16 are considered valid by the RFC.
    *
    * @tparam E an * -> * type for the Stream, usually effectful.
    *
    * @return a Pipe from a Stream of Bytes to a Stream of Json values.
    */
  def jsonByteFrame[E[_]](
    charset: Charset
  ): Pipe[E, Byte, Json] =
    frame andThen jsonFrame(charset)
}
