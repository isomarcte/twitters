package io.isomarcte.twitters.circe.stream

// Java Standard Library Imports //

import java.nio.charset.StandardCharsets
import java.nio.file.Path

// General Imports //

import fs2.Pipe
import fs2.Task

import io.circe.Json

import org.scalacheck.Gen
import org.scalacheck.Prop

import org.specs2.Specification

import org.specs2.ScalaCheck

// Local Imports //

import io.isomarcte.twitters.circe.stream.util.StreamTestData

/** Tests for the Circe FS2 integration. */
final class CirceStreamSpec extends Specification with ScalaCheck {

  // Property Tests

  /** A test that ensure reading Streams of Json at various buffer sizes works
    * as expected. This ensures that there are not assumptions being
    * implicitly made around the size of the default frame in a Stream.
    */
  private[this] lazy val bufferSizeProp: Prop =
    Prop.forAll(
      Gen.oneOf(16, 32, 64, 128, 256, 512)
    ){(bufferSize: Int) =>
      val pipe: Pipe[Task, Byte, Json] =
        CirceStream.frame andThen CirceStream.jsonFrame(StandardCharsets.UTF_8)

      StreamTestData.allResourcePaths.toList.map((p: Path) =>
        pipe(
          StreamTestData.streamTestData[Task](bufferSize, p)
        ).runLog.map{_.toList.size must be_>(0)}.unsafeRun
      )
    }

  // Specification //

  def is = s2"""
  CirceStream Decoder Specification

  Should be able to decode streaming data with variable buffer sizes. ${this.bufferSizeProp}
  """
}
