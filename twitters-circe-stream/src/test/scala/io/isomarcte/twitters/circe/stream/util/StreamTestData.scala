package io.isomarcte.twitters.circe.stream.util

// Java Standard Library Imports //

import java.nio.file.Path
import java.nio.file.Paths

// General Imports //

import fs2.Stream
import fs2.util.Suspendable

import fs2.io.file

/** Provides test data. */
trait StreamTestData {

  // Public Values //

  val tweetResourcePaths: Set[Path] = {
    val clazz = this.getClass
    Set(
      "/TweetStream0.txt",
      "/TweetStream1.txt"
    ).map(r => Paths.get(clazz.getResource(r).toURI))
  }

  /** A list of classpath relative test resources. */
  val nonTweetResourcePaths: Set[Path] = {
    val clazz = this.getClass
    Set(
      "/SimpleJson0.txt",
    ).map(r => Paths.get(clazz.getResource(r).toURI))
  }

  val allResourcePaths: Set[Path] =
    this.tweetResourcePaths ++ this.nonTweetResourcePaths

  /** A List of Streams of test data.
    *
    * @param bufferSize the buffer size at which to read in data.
    *
    * @tparam E a suspendable type constructor.
    *
    * @return a List of Streams test data.
    */
  def streamTestData[E[_] : Suspendable](
    bufferSize: Int,
    resource: Path
  ): Stream[E, Byte] =
    file.readAll(resource, bufferSize)
}

/** Concrete instance of test data. */
object StreamTestData extends StreamTestData
