package io.isomarcte.twitters.circe.stream.scalacheck

// Java Standard Library Imports //

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets._

// General Imports //

import org.scalacheck.Gen

object Generators {

  val charsetGen: Gen[Charset] =
    Gen.oneOf(ISO_8859_1, US_ASCII, UTF_16, UTF_16BE, UTF_16LE, UTF_8)

  val jsonCharsetGen: Gen[Charset] =
    Gen.oneOf(UTF_16, UTF_16BE, UTF_16LE, UTF_8)
}
