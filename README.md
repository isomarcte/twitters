# Twitters #

Twitters is a set of utilities and executable for working with the Twitter Streaming API. The primary purpose of this project is to perform some analytics on the sample tweet stream [tweet stream][sample].

# Modules #

There are a number of different modules in the project. Each has its own `README.md` which describes its purpose and general notes about its use.

* `twitters-oauth`
    * This module provides utilities for working with OAuth 1.0a in an idiomatic manner.
* `twitters-model`
    * This module provides a data model for the JSON types received from twitter. Note that it uses Shapeless to perform a large amount of Typeclass generation, thus you may need to increase the `sbt` heapsize to compile this module. Further compilation and testing may take longer than the other modules.
* `twitters-http-client`
    * This module provides http4s types and utilities for connecting to Twitter.
* `twitters-circe-stream`
    * This module provides utilities for parsing twitter streams into fs2 Streams of circe Json.
* `twitters-analytics`
    * This module provides utilities for performing analysis of fs2 Streams of Tweets (from `twitters-model`)
* `twitters-cli`
    * This module provides a simple command line interface for running analytics on a Stream of Tweets.

# Library Choices #

This project makes heavy of the following libraries.

* [cats][cats]
    * For core FP data types and Typeclasses
* [fs2][fs2]
    * For streaming and effect types.
* [scodec-bits][scodec-bits]
    * For fast, safe, binary operations.
* [spire][spire]
    * For numeric Typeclasses, unsigned data types, and arbitrary precision numbers.

# Running #

For information on running the command line executable, see the [twitters-cli](./twitters-cli "Twitters-cli")

[sample]: https://developer.twitter.com/en/docs/tweets/sample-realtime/overview/GET_statuse_sample "Sample Tweet Stream"

[cats]: https://github.com/typelevel/cats "cats"

[spire]: https://github.com/non/spire "spire"

[fs2]: https://github.com/functional-streams-for-scala/fs2 "fs2"

[scodec-bits]: https://github.com/scodec/scodec-bits "scodec-bits"
