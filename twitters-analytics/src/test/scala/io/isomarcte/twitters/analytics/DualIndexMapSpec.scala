package io.isomarcte.twitters.analytics

// General Imports //

import org.specs2.Specification
import org.specs2.ScalaCheck

/** Tests for the [[DualIndexMap]]. */
final class DualIndexMapSpec extends Specification with ScalaCheck {

  // Private Values //

  // Unit Tests

  /** A Unit Test which ensures that the [[DualIndexMap]] keeps proper track
    * of the quantity of values and that they are yield in the correct order.
    */
  private[this] val unitTest0 = {
    val l: List[Int] = List(1,1,1,2,3,3)
    val result: List[Int] =
      l.foldLeft(DualIndexMap.empty[Int])(
        (acc, value) =>
        acc.succ(value)
      ).valuesByOccurrenceRate(4)
    result must_== List(1,3,2)
  }
  // Specification //

  def is = s2"""
  DualIndexMap Specification

  Enumerating the values should be done by occurrence rate. ${this.unitTest0}
  """
}
