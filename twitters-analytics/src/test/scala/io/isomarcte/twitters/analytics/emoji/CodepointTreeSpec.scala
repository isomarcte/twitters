package io.isomarcte.twitters.analytics.emoji

// General Imports //

import cats.data._

import org.specs2.Specification

/** Tests for the [[CodepointTree]]. */
final class CodepointTreeSpec extends Specification {

  // Private Values //

  /** A [[CodepointTree]] for testing.
    *
    * @note this tree intentionally has overlapping key sequences. For
    * instannce, given the sequence (1,2,1,1,2) the result should be
    *  ("derp", "foobar", "derp"). This is because only the longest run is
    * considered a match, so 1,2 will yield "derp" and just a 1 (not followed
    * by a 2) should yield "foobar".
    */
  private[this] final val tree: CodepointTree[String, Int] =
    CodepointTree.empty[String, Int].insert(
      NonEmptyList.of(1, 2),
      "derp"
    ).insert(
      NonEmptyList.of(1),
      "foobar"
    )

  /** A test that the [[CodepointTree]] detects the expected values with
    * overlapping key sequences.
    */
  private[this] val unitTest0 = {
    (this.tree.results(
      List(1)
    ) must_== List("foobar")) and
    (this.tree.results(
      List(1, 2)
    ) must_== List("derp")) and
    (this.tree.results(
      List(1, 2, 1)
    ).toSet must_== Set("derp", "foobar"))
  }

  // Specification //

  def is = s2"""
  CodepointTree Specification

  The longest valid codepoint sequence should be returned in the results. ${this.unitTest0}
  """
}
