package io.isomarcte.twitters.analytics.emoji

// General Imports //

import cats.data._

/** A [[CodepointTree]] is a specialized Rose Tree
  * [[https://en.wikipedia.org/wiki/Rose_tree]] where there may or may not be
  * a value at each node.
  *
  * It is used to efficiently detect multi codepoint entities in a String,
  * such as [[Emoji]]. This representation will yield amortized O(n) time for
  * detecting all the multi codepoint entities in a String. Where n is the
  * number of codepoints in the String.
  *
  * @note consideration of the [[CodepointNode]] is required to view this type
  *       as a Rose tree. The primary reason for having both [[CodepointTree]]
  *       and [[CodepointNode]] is because our use case has no single root
  *       node, but is rather a forest.
  *
  * @constructor
  * @param bs a Map modeling all the codepoint values accessible at this level
  *        of the tree.
  */
final case class CodepointTree[A, B](
  bs: Map[B, CodepointNode[A, B]]
) extends AnyVal {

  /** Given an NonEmptyList representing a codepoint sequence key, insert the
    * value into the tree at the terminal of the NonEmptyList key sequence.
    *
    * @param bs a NonEmptyList of keys. Each key will map to a new level in
    *        the tree.
    * @param a the value to insert into the tree.
    *
    * @return a new [[CodepointTree]] with the value inserted at the terminal
    *         of the key sequence.
    */
  def insert(bs: NonEmptyList[B], a: A): CodepointTree[A, B] =
    CodepointTree.insert(this)(bs, a)

  /** Given a sequence of values which can be keys in the [[CodepointTree]]
    * return all the values in the sequence which are also in the tree.
    *
    * @param l the list of values to use to search for matching sequences in
    *        the [[CodepointTree]].
    *
    * @return a list of all the matching values found in this
    *         [[CodepointTree]].
    */
  def results(l: List[B]): List[A] =
    CodepointTree.results(this)(l)
}

/** Functions for operating on a [[CodepointTree]]. */
object CodepointTree {

  /** Smart constructor for creating an empty [[CodepointTree]]. */
  def empty[A, B]: CodepointTree[A, B] =
    CodepointTree(Map.empty)

  /** Given an NonEmptyList representing a codepoint sequence key, insert the
    * value into the tree at the terminal of the NonEmptyList key sequence.
    *
    * @param tree the [[CodepointTree]] on which to operate.
    *
    * @param bs a NonEmptyList of keys. Each key will map to a new level in
    *        the tree.
    * @param a the value to insert into the tree.
    *
    * @return a new [[CodepointTree]] with the value inserted at the terminal
    *         of the key sequence.
    */
  def insert[A, B](
    tree: CodepointTree[A, B]
  )(
    bs: NonEmptyList[B],
    a: A
  ): CodepointTree[A, B] =
    bs match {
      case NonEmptyList(h, Nil) =>
        tree.copy(
          bs = tree.bs + (h ->
            tree.bs.get(h).map(n =>
              n.copy(a = Some(a))
            ).getOrElse(
              CodepointNode(
                Some(a),
                this.empty))
          )
        )
      case NonEmptyList(h, t :: ts) => {
        val nextNel: NonEmptyList[B] = NonEmptyList.of(t, ts: _*)
        tree.copy(
          bs = tree.bs + (h ->
            tree.bs.get(h).map(n =>
              n.copy(
                tree = CodepointTree.insert(n.tree)(nextNel, a)
              )
            ).getOrElse(
              CodepointNode(
                Option.empty,
                CodepointTree.insert(
                  this.empty
                )(
                  nextNel,
                  a
                )
              )
            )
          )
        )
      }
    }

  /** Given a sequence of values which can be keys in the [[CodepointTree]]
    * return all the values in the sequence which are also in the tree.
    *
    * @param tree the [[CodepointTree]] on which to operate.
    *
    * @param l the list of values to use to search for matching sequences in
    *        the [[CodepointTree]].
    *
    * @return a list of all the matching values found in this
    *         [[CodepointTree]].
    */
  def results[A, B](
    tree: CodepointTree[A, B]
  )(
    l: List[B]
  ): List[A] =
    l.foldLeft((
      Option.empty[A], // Candidate
      Option.empty[CodepointNode[A, B]], // Cursor
      List.empty[A]) // Accumulator
    ){
      case ((v, Some(n), acc), value) =>
        n.tree.bs.get(value) match {
          case None =>
            val ncur = tree.bs.get(value)
            val ncan = ncur.flatMap(_.a)
            (ncan, ncur, v.toList ++ acc)
          case c @ Some(n) =>
            n.a match {
              case None =>
                (v, c, acc)
              case v @ Some(_) =>
                (v, c, acc)
            }
        }
      case ((_, _, acc), value) =>
        tree.bs.get(value) match {
          case None =>
            (None, None, acc)
          case v @ Some(n) =>
            (n.a, v, acc)
        }
    } match {
      case (Some(v), _, result) =>
        v +: result
      case (_, _, result) =>
        result
    }
}
