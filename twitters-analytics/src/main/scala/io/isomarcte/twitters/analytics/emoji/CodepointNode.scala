package io.isomarcte.twitters.analytics.emoji

/** A [[CodepointNode]] is effectively the node of a specialized Rose Tree
  * where there may or may not be a value at the given node. See
  * [[CodepointTree]] for a full description of the structure and the rational
  * for its use.
  *
  * @constructor
  * @param a the value of the node, which may be empty.
  * @param tree the branches reachable from this node in the
  *        [[CodepointTree]].
  *
  * @tparam A the value type of the [[CodepointNode]].
  * @tparam B the key type of the [[CodepointNode]].
  */
final case class CodepointNode[A, B](
  a: Option[A],
  tree: CodepointTree[A, B]
)
