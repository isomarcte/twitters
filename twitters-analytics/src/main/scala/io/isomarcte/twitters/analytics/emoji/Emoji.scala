package io.isomarcte.twitters.analytics.emoji

// Java Standard Library Imports //

import java.nio.charset.StandardCharsets

// Scala Standard Library Imports //

import scala.io.Source
import scala.util.Failure
import scala.util.Try

// General Imports //

import cats.data._
import cats.implicits._

import io.circe._
import io.circe.parser._

import fs2.util.Effect
import fs2.util.syntax.FunctorOps
import fs2.util.syntax.MonadOps

import spire.math.Natural

// Local Imports //

import io.isomarcte.twitters.analytics.DualIndexMap

/** A record type for an Emoji.
  *
  * @constructor
  * @param name the name of the Emoji.
  * @param codePoints a Set of Codepoints which correspond to this
  *        Emoji. Emoji variations, such as skin color variations, are treated
  *        as the same Emoji under this mechanism.
  */
final case class Emoji(
  name: String,
  codePoints: Set[NonEmptyList[Int]]
)

/** Functions for loading and parsing Emoji data from the classpath. */
object Emoji {

  // Private //

  private[this] val unifiedString: "unified" = "unified"

  /** A function which yields the name of an emoji given JSON representing
    * that emoji.
    *
    * @note if the "name" field is null, then the "short_name" field is used.
    *
    * @param c the HCursor for the decoding operation.
    *
    * @return a Decoder Result which yields the name of the Emoji.
    */
  private[this] def emojiName(c: HCursor): Decoder.Result[String] =
    c.downField("name").as[String] match {
      case r @ Right(_) => r
      case _ => c.downField("short_name").as[String]
    }

  /** Extract the codepoints for any skin variations of the Emoji.
    *
    * @param j a JsonObject representing the skin variations.
    *
    * @return a Decoder Result of a Set of the codepoints for the skin
    *         variant ions of the emoji. If there are no variations this will
    *         be the empty set.
    */
  private[this] def skinVariations(j: JsonObject): Decoder.Result[Set[NonEmptyList[Int]]] =
    j.values.foldLeft(Right(Set.empty[NonEmptyList[Int]]): Either[DecodingFailure, Set[NonEmptyList[Int]]]){
      case (l @ Left(_), _) => l
      case (Right(acc), value) =>
        (for {
          jo <- value.asObject
          unified <- jo(unifiedString)
          us <- unified.asString
          cp <- this.codePointStringToCodePoints(us).toOption
        } yield acc + cp) match {
          case None => Left(DecodingFailure("Unable to read Skin Variantion", Nil))
          case Some(acc) => Right(acc)
        }
    }

  /** Extract all the codepoint sequences from the emoji JSON.
    *
    * @note this extracts all codepoint sequences, including variations.
    *
    * @param c the HCursor for the decoding operation.
    *
    * @return a Decoder Result of a Set of codepoint sequences
    */
  private[this] def codePoints(c: HCursor): Decoder.Result[Set[NonEmptyList[Int]]] =
    for {
      unified <- c.downField(unifiedString).as[String]
      unifiedNel <- this.codePointStringToCodePoints(unified)
      nonQualified <- c.downField("non_qualified").as[Option[String]]
      nonQualifiedNel <- nonQualified.traverse(this.codePointStringToCodePoints)
      jo <- c.downField("skin_variations").as[Option[JsonObject]]
      svNelSet <- jo.traverse(this.skinVariations)
    } yield svNelSet.getOrElse(Set.empty) ++ Set(unifiedNel) ++ nonQualifiedNel.map(a => Set(a)).getOrElse(Set.empty)

  /** A specialized Decoder instance for [[Emoji]] which reads an [[Emoji]]
    * value from the emoji JSON on the classpath.
    */
  private[this] implicit val emojiDecoder: Decoder[Emoji] =
    new Decoder[Emoji] {
      override final def apply(c: HCursor): Decoder.Result[Emoji] =
        for {
          name <- emojiName(c)
          codePoints <- codePoints(c)
        } yield Emoji(name, codePoints)
    }

  /** Given the emoji JSON String representation of the codepoints, convert it
    * into a NonEmptyList of Int codepoints.
    *
    * @param codePointString the String which represents the codepoints.
    *
    * @return a Decoder Result of a NonEmptyList of Int codepoints.
    */
  private[this] def codePointStringToCodePoints(
    codePointString: String
  ): Decoder.Result[NonEmptyList[Int]] =
    (codePointString.split("""-""").toList match {
      case Nil => Failure(new IllegalStateException("Empty list of code points"))
      case h :: t => NonEmptyList.of(h, t: _*).traverse((s: String) => Try(Integer.parseInt(s, 16)))
    }).toEither.leftMap(DecodingFailure.fromThrowable(_, Nil))

  // Public Functions //

  /** Read all the [[Emoji]] values from emoji_pretty.json file on the
    * classpath.
    *
    * @note because this is an effect you should ensure you cache this value
    *       rather than continually execute it.
    *
    * @tparam E an effect type.
    *
    * @return an effect which will yield the Set of all [[Emoji]] value when
    * run.
    */
  def emojis[E[_] : Effect]: E[Set[Emoji]] = {
    val effectInstance: Effect[E] = Effect[E]
    for {
      bufferedSource <- effectInstance.delay(Source.fromInputStream(this.getClass.getResourceAsStream("/emoji_pretty.json"), StandardCharsets.UTF_8.toString))
      string <- effectInstance.delay(bufferedSource.mkString)
      _ <- effectInstance.delay(bufferedSource.close)
      result <- effectInstance.delay(
        (for {
          j <- parse(string)
          js <- Decoder[Vector[Emoji]].decodeJson(j)
        } yield js.toSet).leftMap(t => throw t).merge)
      } yield result
    }

  /** Create the [[CodepointTree]] for [[Emoji]] values.
    *
    * @note because this is an effect you should ensure you cache this value
    *       rather than continually execute it.
    *
    * This is used to efficiently detect [[Emoji]] values in a sequence of
    * unicode codepoints.
    *
    * @tparam E an effect type.
    *
    * @return an effect that will yield a [[CodepointTree]] for [[Emoji]] when
    *         run.
    */
  def codepointTree[E[_] : Effect]: E[CodepointTree[Emoji, Int]] =
    for {
      e <- this.emojis
    } yield e.foldLeft(CodepointTree.empty[Emoji, Int])(
      (acc, emoji) =>
        emoji.codePoints.foldLeft(acc)(
          (acc, value) =>
          CodepointTree.insert(acc)(value, emoji)
        )
    )

  /** Given a String, a [[CodepointTree]] for [[Emoji]] values, and a
    * [[DualIndexMap]] for [[Emoji]] values, update the [[DualIndexMap]] for
    * all [[Emoji]] found in the String.
    *
    * @param t the [[CodepointTree]] used to efficiently test for [[Emoji]] in
    *        the String.
    *
    * @param init the [[DualIndexMap]] to update with any newly found
    *        [[Emoji]] values.
    * @param s the String to search for [[Emoji]] values.
    *
    * @return a pair of the updated [[DualIndexMap]] and a Natural indicating
    *         the quantity of [[Emoji]] found in the given String.
    */
  def emojiInString(
    t: CodepointTree[Emoji, Int]
  )(
    init: DualIndexMap[Emoji],
    s: String
  ): (DualIndexMap[Emoji], Natural) =
    CodepointTree.results[Emoji, Int](t)(
      s.codePoints().toArray().toList
    ).foldLeft((init, Natural.NaturalAlgebra.zero)){
      case ((acc, count), value) => (acc.succ(value), count + Natural.NaturalAlgebra.one)
    }
}
