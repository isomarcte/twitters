package io.isomarcte.twitters.analytics

// Java Standard Library Imports //

import java.time.Duration
import java.time.Instant
import java.util.regex.Pattern
import java.util.regex.Matcher

// Scala Standard Library Imports //

import scala.BigDecimal

// General Imports //

import cats.implicits._

import fs2.Stream
import fs2.util.Effect
import fs2.util.Suspendable
import fs2.util.syntax.FunctorOps
import fs2.util.syntax.MonadOps

import spire.math.Fractional
import spire.math.Natural
import spire.math.Natural.NaturalAlgebra.one
import spire.math.Natural.NaturalAlgebra.zero

// Module Imports //

import io.isomarcte.twitters.model._

// Local Imports //

import io.isomarcte.twitters.analytics.emoji._

/** A record for keeping track of the data points we wish to analyze.
  *
  * @constructor
  * @param tweetCount the total number of tweets seen in this run.
  * @param startTime the time at which we began reading tweets.
  * @param codepointTree a [[CodepointTree]] used to check for the presence of
  *        emoji in a tweet.
  * @param emojiOccurrences a [[DualIndexMap]] used to efficiently track the
  *        number of occurrences of a given emoji.
  * @param hashtagOccurrences a [[DualIndexMap]] used to efficiently track the
  *        number of occurrences of a given hashtag.
  * @param urlOccurrences a [[DualIndexMap]] used to efficiently track the
  *        number of occurrences of a given domain.
  * @param tweetsWithUrlCount the number of tweets which contained at least
  *        ''one'' URL.
  * @param tweetsWithEmojiCount the number of tweets which contained at least
  *        ''one'' emoji.
  * @param tweetsWithPhotoDomain the number of tweets which contained a link
  *        to a photo domain.
  */
final case class Analytics(
  tweetCount: Natural,
  startTime: Instant,
  codepointTree: CodepointTree[Emoji, Int],
  emojiOccurrences: DualIndexMap[Emoji],
  hashtagOccurrences: DualIndexMap[String],
  urlOccurrences: DualIndexMap[String],
  tweetsWithUrlCount: Natural,
  tweetsWithEmojiCount: Natural,
  tweetsWithPhotoDomain: Natural
)

/** Functions working with [[Analtyics]] */
object Analytics {

  // Private Values //

  /** Name of the regex capturing group for the domain of a URL. */
  private[this] val domainGroupName: "domain" = "domain"

  /** A regex pattern for extracting the domain from a URL like String. This
    * is required because many of the fields which are said to contain URL
    * values in the twitter API often are missing a scheme, e.g. https, but
    * not always.
    */
  private[this] val domainPattern: Pattern =
    Pattern.compile("""http[s]?://(www\.)?(?<""" |+| this.domainGroupName |+| """>[^/]*).*""")

  /** A Set of the domains which are considered photo domains. */
  private[this] val photoDomains: Set[String] =
    Set("pic.twitter.com", "instagram.com")

  // Private Functions //

  /** Create an effect which will yield the current Duration spent reading
    * from this stream.
    *
    * @param analytics the [[Analtyics]] value for the current stream.
    *
    * @tparam E a Suspendable type.
    *
    * @return an effect that will yield the duration reading from the stream
    *         when run.
    */
  private[this] def durationReadingStream[E[_] : Suspendable](
    analytics: Analytics
  ): E[Duration] =
    for {
      now <- Suspendable[E].delay(Instant.now)
    } yield Duration.between(analytics.startTime, now)

  /** Create an effect which will yield the amount of tweets read per second.
    *
    * @param analytics the [[Analtyics]] value for the current stream.
    *
    * @tparam E a Suspendable type.
    * @tparam A a Fractional type.
    *
    * @param an effect which will yield the tweets/second as a Fractional
    *        value when run.
    */
  private[this] def tweetsPerSecond[E[_] : Suspendable, A : Fractional](
    analytics: Analytics
  ): E[A] = {
    val S: Suspendable[E] = implicitly[Suspendable[E]]
    val F: Fractional[A] = Fractional[A]
    for {
      now <- S.delay(Instant.now)
      duration <- this.durationReadingStream(analytics)
    } yield
      F.div(
        F.fromBigInt(analytics.tweetCount.toBigInt),
        F.fromLong(duration.getSeconds)
      )
  }

  /** Calculate the ratio of tweets with emoji values to all tweets.
    *
    * @param analytics the [[Analtyics]] value for this stream.
    *
    * @tparam A a Fractional type.
    *
    * @return a ratio of tweets with emoji to all tweets.
    */
  private[this] def tweetsWithEmojiRatio[A : Fractional](
    analytics: Analytics
  ): A = {
    val F: Fractional[A] = Fractional[A]
    F.times(
      F.div(
        F.fromBigInt(analytics.tweetsWithEmojiCount.toBigInt),
        F.fromBigInt(analytics.tweetCount.toBigInt)
      ),
      F.fromInt(100)
    )
  }

  /** Update the hashtag occurrences for a given [[Tweet]] in a given
    * [[Analtyics]] value.
    *
    * @param tweet the [[Tweet]] from which to extract the hashtag occurrences.
    * @param analytics the [[Analtyics]] value to update.
    *
    * @return a new [[Analtyics]] value with updates made for any hashtags in
    *         the given [[Tweet]].
    */
  private[this] def hashtagOccurrences(
    tweet: Tweet,
    analytics: Analytics
  ): Analytics =
    analytics.copy(
      hashtagOccurrences =
        tweet.entities.hashtags.toVector.flatMap(
          identity
        ).foldLeft(analytics.hashtagOccurrences)(
          (acc, value) => acc.succ(value.text)
        )
    )

  /** Perform all of the analytics.
    *
    * This function performs all of the analytics on the given [[Tweet]] and
    * yields an update [[Analtyics]] state.
    *
    * @param analytics an [[Analtyics]] value to update at this step.
    * @param tweet the current tweet to analyze.
    *
    * @return the updated [[Analtyics]] value.
    */
  private[this] def analyze(
    analytics: Analytics,
    tweet: Tweet
  ): Analytics =
    this.urlsInTweet(
      tweet,
      this.hashtagOccurrences(
        tweet,
        this.emojiInTweet(
          tweet,
          this.incrementTweetCount(analytics)
        )))

  /** Update the emoji occurrences for a given [[Tweet]] in a given
    * [[Analtyics]] value.
    *
    * @param tweet the [[Tweet]] from which to extract the emoji information.
    * @param analytics the [[Analtyics]] value to update.
    *
    * @return a new [[Analtyics]] value with updates made for any emoji in
    *         the given [[Tweet]].
    */
  private[this] def emojiInTweet(
    tweet: Tweet,
    analytics: Analytics
  ): Analytics =
    Emoji.emojiInString(
      analytics.codepointTree
    )(
      analytics.emojiOccurrences,
      tweet.text
    ) match {
      case (updated, count) if count === zero =>
        assert(updated === analytics.emojiOccurrences)
        analytics
      case (updated, count) =>
        assert(count > zero)
        analytics.copy(
          emojiOccurrences = updated,
          tweetsWithEmojiCount = analytics.tweetsWithEmojiCount + one
        )
    }

  /** Given a String, treat that String as a URL like value and attempt to
    * extract the domain from it.
    *
    * @param s the String from which to extract the domain.
    *
    * @param M a MonadError instance for E in Throwable.
    *
    * @tparam E a MonadError value in Throwable.
    *
    * @return the domain portion of that String or an error in the Monad.
    */
  private[this] def urlDomain(
    s: String
  ): String = {
    val m: Matcher = this.domainPattern.matcher(s)
    if (m.matches) {
      m.group(this.domainGroupName)
    } else {
      throw new AssertionError(
        s"s does not contain any domain. This is not possible."
      )
    }
  }

  /** Checks to see if a String representing a domain is considered a photo
    * domain.
    *
    * @param s the String to check to see if it is a photo domain.
    *
    * @return true if the String is a photo domain, false otherwise.
    */
  private[this] def isPhotoDomain(
    s: String
  ): Boolean =
    this.photoDomains.contains(s)

  /** Update the url occurrences for a given [[Tweet]] in a given
    * [[Analtyics]] value.
    *
    * @param tweet the [[Tweet]] from which to extract the url information.
    * @param analytics the [[Analtyics]] value to update.
    *
    * @return a new [[Analtyics]] value with updates made for any url in
    *         the given [[Tweet]].
    */
  private[this] def urlsInTweet(
    tweet: Tweet,
    analytics: Analytics
  ): Analytics =
    tweet.entities.urls.toVector.flatMap(
      identity
    ).foldLeft((analytics.urlOccurrences, zero, zero)){
      case ((acc, photoDomainCount, count), value) =>
        val domain = this.urlDomain(value.expanded_url)
        if (this.isPhotoDomain(domain)) {
          (acc.succ(domain), photoDomainCount + one, count + one)
        } else {
          (acc.succ(domain), photoDomainCount, count + one)
        }
    } match {
      case (updates, photoDomainCount, count) if count === zero =>
        assert(updates === analytics.urlOccurrences)
        assert(photoDomainCount === zero)
        analytics
      case (updates, photoDomainCount, count) =>
        assert(count > zero)
        assert(photoDomainCount <= count)
        val photoInc: Natural =
          if (photoDomainCount > zero) one else zero
        analytics.copy(
          urlOccurrences = updates,
          tweetsWithUrlCount = analytics.tweetsWithUrlCount + one,
          tweetsWithPhotoDomain = analytics.tweetsWithPhotoDomain + photoInc
        )
    }

  /** Calculate the ratio of tweets with URLs to all tweets.
    *
    * @param analytics the [[Analtyics]] value for this stream.
    *
    * @tparam A a Fractional type.
    *
    * @return a ratio of tweets with URLs to all tweets.
    */
  private[this] def tweetsWithUrlRatio[A : Fractional](
    analytics: Analytics
  ): A = {
    val F: Fractional[A] = Fractional[A]
    F.times(
      F.div(
        F.fromBigInt(analytics.tweetsWithUrlCount.toBigInt),
        F.fromBigInt(analytics.tweetCount.toBigInt)
      ),
      F.fromInt(100)
    )
  }

  /** Calculate the ratio of tweets with photo domain URLs to all tweets.
    *
    * @param analytics the [[Analtyics]] value for this stream.
    *
    * @tparam A a Fractional type.
    *
    * @return a ratio of tweets with photo domain URLs to all tweets.
    */
  private[this] def tweetsWithPhototDomainRatio[A : Fractional](
    analytics: Analytics
  ): A = {
    val F: Fractional[A] = Fractional[A]
    F.times(
      F.div(
        F.fromBigInt(analytics.tweetsWithPhotoDomain.toBigInt),
        F.fromBigInt(analytics.tweetCount.toBigInt)
      ),
      F.fromInt(100)
    )
  }

  /** Increment the number of tweets.
    *
    * @param analytics the [[Analytics]] value to update.
    *
    * @return a new [[Analtyics]] with the number of tweets incremented by
    *         one.
    */
  private[this] def incrementTweetCount(
    analytics: Analytics
  ): Analytics =
    analytics.copy(tweetCount = analytics.tweetCount + one)

  // Public Functions //

  /** Create an effect which will yield an empty [[Analtyics]] value. In
    * particular, the start time of the [[Analtyics]] run will be set to the
    * time when this effect is run, not created.
    *
    * @tparam E an effect type.
    *
    * @return an effect that will yield an empty [[Analtyics]] value when run.
    */
  def empty[E[_] : Effect]: E[Analytics] =
    for {
      now <- Effect[E].delay(Instant.now)
      t <- Emoji.codepointTree[E]
    } yield Analytics(
      zero,
      now,
      t,
      DualIndexMap.empty[Emoji],
      DualIndexMap.empty[String],
      DualIndexMap.empty[String],
      zero,
      zero,
      zero
    )

  /** Given a Stream of [[Tweet]] values perform analytics on them yielding an
    * aggregated [[Analtyics]] value for each [[Tweet]].
    *
    * @param s the Stream of [[Tweet]] values.
    *
    * @return a Stream of [[Analtyics]] computed from the [[Tweet]] values.
    */
  def pipeline[E[_] : Effect](
    s: Stream[E, Tweet]
  ): Stream[E, Analytics] =
    for {
      init <- Stream.eval(Analytics.empty[E])
      result <- s.mapAccumulate(init)((a, t) => this.analyze(a, t) -> t).map(_._1)
    } yield result

  /** Given an [[Analtyics]] value create a String suitable for logging to the
    * terminal which describes the computed analytics of the tweet stream.
    *
    * @param analytics the [[Analtyics]] value for which to create the output
    *        String.
    *
    * @tparam E a Suspendable type constructor.
    *
    * @return an effect which will yield a String representation of the
    *         [[Analtyics]] value suitable for logging to the terminal.
    */
  def toResultString[E[_] : Suspendable](
    analytics: Analytics
  ): E[String] =
    for {
      tps <- this.tweetsPerSecond[E, BigDecimal](analytics)
      duration <- this.durationReadingStream(analytics)
    } yield {
      val tpm: BigDecimal = tps * BigDecimal(60)
      val tph: BigDecimal = tpm * BigDecimal(60)
      val ptwe: BigDecimal =
        this.tweetsWithEmojiRatio[BigDecimal](analytics)
      val ptwu: BigDecimal =
        this.tweetsWithUrlRatio[BigDecimal](analytics)
      val ptwpd: BigDecimal =
        this.tweetsWithPhototDomainRatio[BigDecimal](analytics)
      val count: Int = 5
      val te: List[(Emoji, Natural)] =
        analytics.emojiOccurrences.valuesByOccurrenceRateWithRate(count)
      val tht: List[(String, Natural)] =
        analytics.hashtagOccurrences.valuesByOccurrenceRateWithRate(count)
      val topDomains: List[(String, Natural)] =
        analytics.urlOccurrences.valuesByOccurrenceRateWithRate(count)
      s"""\nTwitter Analtyics (Started at ${analytics.startTime})
        | Tweets Total                                 : ${analytics.tweetCount}
        | Duration Reading Stream                      : $duration
        | Tweets/Second                                : $tps
        | Tweets/Minute                                : $tpm
        | Tweets/Hour                                  : $tph
        | Percentage of Tweets with Emoji              : ${ptwe}%
        | Percentage of Tweets with URLs               : ${ptwu}%
        | Percentage of Tweets with Photo Domain Links : ${ptwpd}%
        | Top $count Domains                                : ${topDomains.mkString(", ")}
        | Top $count HashTags                               : ${tht.mkString(", ")}
        | Top $count Emoji                                  : ${te.mkString(", ")}""".stripMargin
    }
}
