package io.isomarcte.twitters.analytics

// Scala Standard Library Imports //

import scala.collection.SortedMap

// General Imports //

import cats.kernel

import spire.math.Natural

/** A wrapper data structure which allows for efficient tracking of the
  * occurrence rate of some value and efficiently computing the top N values
  * with highest occurrence rate.
  *
  * This is achieved by using two Map data structures. One map is used to
  * track the occurrences of the values, another maps occurrence rates back to
  * Sets of values.
  *
  * @constructor
  * @param elementMap the Map of values to occurrence rates.
  * @param sortedOccurenceMap the SortedMap of occurrence rates to Sets of
  *        values.
  *
  * @tparam A the type of the values being tracked.
  */
final case class DualIndexMap[A](
  elementMap: Map[A, Natural],
  sortedOccurenceMap: SortedMap[Natural, Set[A]]
) {

  /** Yield the top N values in order of occurrence rate (highest first).
    *
    * @param count the number of values to yield.
    *
    * @return a List of the top N values.
    */
  def valuesByOccurrenceRate(
    count: Int
  ): List[A] =
    DualIndexMap.valuesByOccurrenceRate(this, count)

  /** Yield the top N values in order of occurrence rate (highest first).
    *
    * @param count the number of values to yield.
    *
    * @return a List of the top N values, paired with the occurrence rate.
    */
  def valuesByOccurrenceRateWithRate(
    count: Int
  ): List[(A, Natural)] =
    this.valuesByOccurrenceRate(count).map(a =>
      (a, elementMap(a))
    )

  /** For a given value, increment the number of occurrences for this value.
    *
    * @param a the value for which to increment the occurrence rate.
    *
    * @return a new [[DualIndexMap]] with the occurrence rate for the given
    *         value incremented.
    */
  def succ(a: A): DualIndexMap[A] =
    DualIndexMap.succ(this, a)
}

/** Typeclass instances for [[DualIndexMap]]. */
trait DualIndexMapInstances {

  /** Eq instance for [[DualIndexMap]]. */
  implicit final def dualIndexMapEq[A]: kernel.Eq[DualIndexMap[A]] =
    kernel.Eq.fromUniversalEquals
}

/** Functions which operate on [[DualIndexMap]] values. */
object DualIndexMap extends DualIndexMapInstances {

  // Private Values //

  /** We reverse the normal Ordering for Natural because we want the highest
    * occurrence rate to be at the head of the output.
    */
  private[this] implicit val reverseNaturalOrdering: Ordering[Natural] =
    spire.compat.ordering[Natural].reverse

  // Public Functions //

  /** Create an empty [[DualIndexMap]].
    *
    * @tparam A the type of values to track in this [[DualIndexMap]].
    *
    * @return an empty [[DualIndexMap]].
    */
  def empty[A]: DualIndexMap[A] =
    DualIndexMap(Map.empty, SortedMap.empty)

  /** Update the occurrence rate for a given value in a given
    * [[DualIndexMap]].
    *
    * @param m the [[DualIndexMap]] for which to update the occurrence rate.
    * @param a the value for which the occurrence rate should be updated.
    *
    * @tparam A the type of value in the [[DualIndexMap]].
    *
    * @return a new [[DualIndexMap]] with the occurrence rate for the given
    *         value incremented by one.
    */
  def succ[A](
    m: DualIndexMap[A],
    a: A
  ): DualIndexMap[A] = {
    val one: Natural =
      Natural.NaturalAlgebra.one
    val em: Map[A, Natural] =
      m.elementMap
    val om: SortedMap[Natural, Set[A]] =
      m.sortedOccurenceMap
    val oldValue: Option[Natural] =
      em.get(a)
    val newElementMap: Map[A, Natural] =
      em + (a -> oldValue.map(
        _ + one
      ).getOrElse(Natural.NaturalAlgebra.one))
    val newOccurenceMap: SortedMap[Natural, Set[A]] =
      oldValue.map{v =>
        val newV: Natural = v + one
        val oldSet: Set[A] =
          om(v) - a
        val newPair: (Natural, Set[A]) =
          (newV -> om.get(newV).map(_ + a).getOrElse(Set(a)))
        if (oldSet.isEmpty) {
          om - v + newPair
        } else {
          om + (v -> oldSet) + newPair
        }
      }.getOrElse(om + (one -> om.get(one).map(_ + a).getOrElse(Set(a))))
    DualIndexMap(newElementMap, newOccurenceMap)
  }

  /** Yield the top N values with the highest occurrence rate for the given
    * [[DualIndexMap]].
    *
    * @param d the [[DualIndexMap]] for which to yield the top N values.
    * @param count the number of values to yield.
    *
    * @tparam A the type of values being tracked by the [[DualIndexMap]].
    *
    * @return a List of the top N values by occurrence rate.
    */
  def valuesByOccurrenceRate[A](
    d: DualIndexMap[A],
    count: Int
  ): List[A] =
    d.sortedOccurenceMap.values.flatMap(_.toIterable).take(count).toList
}
