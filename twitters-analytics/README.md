# Twitters Analytics ##

This module provides utilities for performing analysis on Tweet values.

The `Analytics` type is the primary entry point to this functionality and methods on the companion object should be the main interface for this module.
