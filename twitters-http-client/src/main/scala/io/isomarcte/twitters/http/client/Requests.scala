package io.isomarcte.twitters.http.client

// Scala Language Features //

import scala.language.higherKinds

// Java Standard Library Imports //

import java.net.URL
import java.nio.charset.StandardCharsets.UTF_8

// Scala Standard Library Imports //

import scala.util.Try
import scala.util.Success
import scala.util.Failure
import scala.collection.SortedMap
import scala.collection.SortedSet

// General Imports //

import cats._
import cats.implicits._

import fs2.Chunk
import fs2.Stream
import fs2.util.Effect

import org.http4s.Header
import org.http4s.HttpVersion
import org.http4s.Method
import org.http4s.Query
import org.http4s.Request
import org.http4s.Uri
import org.http4s.headers.Authorization

// Module Imports //

import io.isomarcte.twitters.oauth.HTTPMethod
import io.isomarcte.twitters.oauth.OAuthConfig
import io.isomarcte.twitters.oauth.OAuthParameters
import io.isomarcte.twitters.oauth.OAuthUtil
import io.isomarcte.twitters.oauth.util.SortedMapUtil

/** Utilities and constants for working with the Twitter API in terms of the
  * http4s client.
  */
object Requests {

  // Public Types //

  /** A type alias for referring to non-encoded form bodies. */
  type FormBody = SortedMap[String, Option[String]]

  // Public Values //

  /** A pre-built request for reading the sample streaming API.
    *
    * @see [[https://developer.twitter.com/en/docs/tweets/sample-realtime/overview/GET_statuse_sample
    */
  val sampleRealtimeTweets: Request =
    Request(
      Method.GET,
      TwitterUris.sampleRealtime,
      HttpVersion.`HTTP/2.0`
    )

  /** A pre-built request for reading the filtered streaming API.
    *
    * @see [[https://developer.twitter.com/en/docs/tweets/filter-realtime/overview]]
    */
  val filterRealtimeTweets: RequestWithBody = {
    val body: FormBody =
      SortedMap(
        TwitterParameters.delimited,
        TwitterParameters.stallWarnings,
      )
    RequestWithBody(
      Request(
        Method.POST,
        TwitterUris.filterReatime,
        HttpVersion.`HTTP/2.0`,
        body = Stream.chunk(Chunk.bytes(SortedMapUtil.formEncode(body).getBytes(UTF_8)))
      ),
      body)
  }

  // Public Functions //

  /** Create an "Authorization" header for a Request.
    *
    * @note existing "Autorization" headers will be replaced.
    *
    * @note the query parameters will be extracted from the Request, but you
    *       will need to provide any body parameters directly. This is to
    *       avoid having to re-parse the parameters out of the body.
    *
    * @param config the [[OAuthConfig]] for OAuth.
    *
    * @param request the request to authorize.
    * @param bodyParameters any body parameters which should be consider
    *        when creating the OAuth signature.
    *
    * @tparam E an effect type which also has a MonadError instance in terms
    *         of Throwable errors.
    *
    * @return an effect which will create a request identical to the input
    *         request except with an "Authorization" header.
    */
  def authorizeRequestWithBody[E[_]](
    config: OAuthConfig
  )(
    requestWithBody: RequestWithBody,
  )(
    implicit
      effectInstance: Effect[E],
      monadErrorInstance: MonadError[E, Throwable]
  ): E[Request] = {
    val request: Request = requestWithBody.request
    val bodyParameters: SortedMap[String, SortedSet[String]] =
      requestWithBody.body.mapValues(v => SortedSet(v.toList: _*))
    lazy val invalidMethodError: Throwable =
      new IllegalArgumentException(s"${request.method} is not a supported method")
    for {
      oauthParams <- OAuthParameters.fromConfig(config)
      allparams = oauthParams |+| OAuthParameters(bodyParameters) |+| this.queryToOAuthParameters(request.uri.query)
      optMethod = MethodOptics.httpMethodPrism.getOption(request.method)
      method <- optMethod.map(_.pure[E]).getOrElse(invalidMethodError.raiseError[E, HTTPMethod])
      url <- this.uriToUrl[E](request.uri.copy(query = Query.empty, fragment = None))
      sig <- OAuthUtil.createSignature(config.oauthSecrets)(method, url, allparams)
    } yield {
      val signedParams: OAuthParameters =
        OAuthParameters(oauthParams.params + sig.asPair)
      val authHeader: Header = this.authorizationHeader(signedParams)
      request.putHeaders(authHeader)
    }
  }

  /** Create an "Authorization" header for a Request.
    *
    * @note existing "Autorization" headers will be replaced.
    *
    * @note this is a convenience function for calling
    *       [[Requests#authorizeRequestWithParemeters]] with no extra
    *       parameters.
    *
    * @param config the [[OAuthConfig]] for OAuth.
    *
    * @param request the request to authorize.
    *
    * @tparam E an effect type which also has a MonadError instance in terms
    *         of Throwable errors.
    *
    * @return an effect which will create a request identical to the input
    *         request except with an "Authorization" header.
    */
  def authorizeRequest[E[_]](
    config: OAuthConfig
  )(
    request: Request
  )(
    implicit
      effectInstance: Effect[E],
      monadErrorInstance: MonadError[E, Throwable]
  ): E[Request] =
    this.authorizeRequestWithBody(
      config,
    )(
      RequestWithBody(
        request,
        SortedMap.empty[String, Option[String]]
      )
    )

  // Private Functions //

  /** Convert a http4s Query into an [[OAuthParameters]] value.
    *
    * @param q the Query.
    *
    * @return the Query represented as [[OAuthParameters]]
    */
  def queryToOAuthParameters(q: Query): OAuthParameters =
    q.toVector.foldMap{
      case (k, Some(v)) => OAuthParameters(SortedMap((k -> SortedSet(v))))
      case _ => OAuthParameters(SortedMap.empty)
    }

  /** Convert a http4s Uri to an Java URL.
    *
    * @param uri the Uri to convert.
    *
    * @tparam E a MonadError type with errors in terms of Throwable values.
    *
    * @return an URL representation of the Uri or an error in the monadic
    *         context.
    */
  private[this] def uriToUrl[E[_]](
    uri: Uri
  )(
    implicit M: MonadError[E, Throwable]
  ): E[URL] =
    Try(new URL(uri.renderString)) match {
      case Success(url) => M.pure(url)
      case Failure(e) => M.raiseError[URL](e)
    }

  /** Create the OAuth "Authorization" header from an [[OAuthParameters]]
    * value.
    *
    * @note the input parameters should contain an "oauth_signature" value.
    *
    * @param parameters the [[OAuthParameters]].
    *
    * @return an http4s "Authorization" Header value.
    */
  private[this] def authorizationHeader(parameters: OAuthParameters): Header =
    Header(Authorization.name.toString, "OAuth " |+|
      parameters.percentEncoded.map{
        case (a, bs) =>
          bs.map(b => a |+| "=" |+| "\"" |+| b |+|  "\"").mkString(", ")
      }.mkString(", ")
    )
}
