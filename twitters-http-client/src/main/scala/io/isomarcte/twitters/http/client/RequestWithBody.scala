package io.isomarcte.twitters.http.client

// Scala Standard Library Imports //

import scala.collection.SortedMap

// General Imports //

import org.http4s.Request

/** A simple wrapper type for a Request and the body of that request to be
  * form encoded.
  *
  * @note this primary use for this type is to keep a reference to the
  *       non-encoded body until after the OAuth signature has been generated,
  *       as the generation of said signature requires the body.
  *
  * @constructor
  * @param request a request.
  * @param body the body of the request which will be form encoded into the request.
  */
final case class RequestWithBody(
  request: Request,
  body: SortedMap[String, Option[String]]
)
