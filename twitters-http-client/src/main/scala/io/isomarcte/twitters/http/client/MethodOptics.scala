package io.isomarcte.twitters.http.client

// General Imports //

import cats.implicits._

import monocle.Prism

import org.http4s.Method

// Module Imports //

import io.isomarcte.twitters.oauth.HTTPMethod

/** Optics for working with HTTP methods. */
object MethodOptics {

  /** A Prism between [[HTTPMethod]] and the http4s method type.
    *
    * @note this can not be a true Isomorphism because of Method.fromString in http4s.
    */
  val httpMethodPrism: Prism[Method, HTTPMethod] =
    Prism[Method, HTTPMethod]{
      case Method.ACL => HTTPMethod.ACL.pure[Option]
      case Method.`BASELINE-CONTROL` => HTTPMethod.`BASELINE-CONTROL`.pure[Option]
      case Method.BIND => HTTPMethod.BIND.pure[Option]
      case Method.CHECKIN => HTTPMethod.CHECKIN.pure[Option]
      case Method.CHECKOUT => HTTPMethod.CHECKOUT.pure[Option]
      case Method.CONNECT => HTTPMethod.CONNECT.pure[Option]
      case Method.COPY => HTTPMethod.COPY.pure[Option]
      case Method.DELETE => HTTPMethod.DELETE.pure[Option]
      case Method.GET => HTTPMethod.GET.pure[Option]
      case Method.HEAD => HTTPMethod.HEAD.pure[Option]
      case Method.LABEL => HTTPMethod.LABEL.pure[Option]
      case Method.LINK => HTTPMethod.LINK.pure[Option]
      case Method.LOCK => HTTPMethod.LOCK.pure[Option]
      case Method.MERGE => HTTPMethod.MERGE.pure[Option]
      case Method.MKACTIVITY => HTTPMethod.MKACTIVITY.pure[Option]
      case Method.MKCALENDAR => HTTPMethod.MKCALENDAR.pure[Option]
      case Method.MKCOL => HTTPMethod.MKCOL.pure[Option]
      case Method.MKREDIRECTREF => HTTPMethod.MKREDIRECTREF.pure[Option]
      case Method.MKWORKSPACE => HTTPMethod.MKWORKSPACE.pure[Option]
      case Method.MOVE => HTTPMethod.MOVE.pure[Option]
      case Method.OPTIONS => HTTPMethod.OPTIONS.pure[Option]
      case Method.ORDERPATCH => HTTPMethod.ORDERPATCH.pure[Option]
      case Method.PATCH => HTTPMethod.PATCH.pure[Option]
      case Method.POST => HTTPMethod.POST.pure[Option]
      case Method.PROPFIND => HTTPMethod.PROPFIND.pure[Option]
      case Method.PROPPATCH => HTTPMethod.PROPPATCH.pure[Option]
      case Method.PUT => HTTPMethod.PUT.pure[Option]
      case Method.REBIND => HTTPMethod.REBIND.pure[Option]
      case Method.REPORT => HTTPMethod.REPORT.pure[Option]
      case Method.SEARCH => HTTPMethod.SEARCH.pure[Option]
      case Method.TRACE => HTTPMethod.TRACE.pure[Option]
      case Method.UNBIND => HTTPMethod.UNBIND.pure[Option]
      case Method.UNCHECKOUT => HTTPMethod.UNCHECKOUT.pure[Option]
      case Method.UNLINK => HTTPMethod.UNLINK.pure[Option]
      case Method.UNLOCK => HTTPMethod.UNLOCK.pure[Option]
      case Method.UPDATE => HTTPMethod.UPDATE.pure[Option]
      case Method.UPDATEREDIRECTREF => HTTPMethod.UPDATEREDIRECTREF.pure[Option]
      case Method.`VERSION-CONTROL` => HTTPMethod.`VERSION-CONTROL`.pure[Option]
      case _ => ().raiseError[Option, HTTPMethod]
    }{
      case HTTPMethod.ACL => Method.ACL
      case HTTPMethod.`BASELINE-CONTROL` => Method.`BASELINE-CONTROL`
      case HTTPMethod.BIND => Method.BIND
      case HTTPMethod.CHECKIN => Method.CHECKIN
      case HTTPMethod.CHECKOUT => Method.CHECKOUT
      case HTTPMethod.CONNECT => Method.CONNECT
      case HTTPMethod.COPY => Method.COPY
      case HTTPMethod.DELETE => Method.DELETE
      case HTTPMethod.GET => Method.GET
      case HTTPMethod.HEAD => Method.HEAD
      case HTTPMethod.LABEL => Method.LABEL
      case HTTPMethod.LINK => Method.LINK
      case HTTPMethod.LOCK => Method.LOCK
      case HTTPMethod.MERGE => Method.MERGE
      case HTTPMethod.MKACTIVITY => Method.MKACTIVITY
      case HTTPMethod.MKCALENDAR => Method.MKCALENDAR
      case HTTPMethod.MKCOL => Method.MKCOL
      case HTTPMethod.MKREDIRECTREF => Method.MKREDIRECTREF
      case HTTPMethod.MKWORKSPACE => Method.MKWORKSPACE
      case HTTPMethod.MOVE => Method.MOVE
      case HTTPMethod.OPTIONS => Method.OPTIONS
      case HTTPMethod.ORDERPATCH => Method.ORDERPATCH
      case HTTPMethod.PATCH => Method.PATCH
      case HTTPMethod.POST => Method.POST
      case HTTPMethod.PROPFIND => Method.PROPFIND
      case HTTPMethod.PROPPATCH => Method.PROPPATCH
      case HTTPMethod.PUT => Method.PUT
      case HTTPMethod.REBIND => Method.REBIND
      case HTTPMethod.REPORT => Method.REPORT
      case HTTPMethod.SEARCH => Method.SEARCH
      case HTTPMethod.TRACE => Method.TRACE
      case HTTPMethod.UNBIND => Method.UNBIND
      case HTTPMethod.UNCHECKOUT => Method.UNCHECKOUT
      case HTTPMethod.UNLINK => Method.UNLINK
      case HTTPMethod.UNLOCK => Method.UNLOCK
      case HTTPMethod.UPDATE => Method.UPDATE
      case HTTPMethod.UPDATEREDIRECTREF => Method.UPDATEREDIRECTREF
      case HTTPMethod.`VERSION-CONTROL` => Method.`VERSION-CONTROL`
    }
}
