package io.isomarcte.twitters.http.client

// General Imports //

import org.http4s.Query
import org.http4s.Uri

/** Constant URIs for the Twitter API endpoints. */
object TwitterUris {

  /** The endpoint for a sample set of the streaming realtime tweets.
    *
    * @see [[https://developer.twitter.com/en/docs/tweets/sample-realtime/overview/GET_statuse_sample]]
    */
  val sampleRealtime: Uri =
    Uri.uri("https://stream.twitter.com/1.1/statuses/sample.json").copy(
      query = Query(
        TwitterParameters.stallWarnings
      )
    )

  /** The endpoint for a the filter streaming API.
    *
    * @see [[https://developer.twitter.com/en/docs/tweets/filter-realtime/api-reference/post-statuses-filter.html]]
    */
  val filterReatime: Uri =
    Uri.uri("https://stream.twitter.com/1.1/statuses/filter.json").copy(
      query = Query(
        TwitterParameters.delimited,
        TwitterParameters.stallWarnings
      )
    )
}
