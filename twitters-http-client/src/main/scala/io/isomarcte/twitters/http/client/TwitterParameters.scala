package io.isomarcte.twitters.http.client

// General Imports //

import cats.implicits._

/** Parameters which are commonly added to Twitter API calls. */
object TwitterParameters {

  // Public Types //

  /** A Parameter is just a pair. */
  type Parameter = (String, Option[String])

  // Used in the Streaming API.
  val delimited: Parameter = ("delimited", "length".pure[Option])
  val stallWarnings: Parameter = ("stall_warnings", true.show.pure[Option])
}
