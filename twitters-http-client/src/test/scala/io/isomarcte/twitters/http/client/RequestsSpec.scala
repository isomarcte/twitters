package io.isomarcte.twitters.http.client

// Java Standard Library Imports //

import java.nio.charset.StandardCharsets.UTF_8

// Scala Standard Library Imports //

import scala.collection.SortedMap

// General Imports //

import cats.implicits._

import fs2.Chunk
import fs2.Stream
import fs2.Task

import fs2.interop.cats._

import org.http4s.HttpVersion
import org.http4s.Method
import org.http4s.Request
import org.http4s.Uri
import org.http4s.headers.Authorization

import org.http4s.testing.ArbitraryInstances.{
  arbitraryUri => _,
  arbitraryMethod => _, _}

import org.scalacheck.Arbitrary
import org.scalacheck.Gen
import org.scalacheck.Prop

import org.scalacheck.ScalacheckShapeless._

import org.specs2.Specification
import org.specs2.ScalaCheck

// Module Imports //

import io.isomarcte.twitters.oauth.ConsumerKey
import io.isomarcte.twitters.oauth.OAuthConfig
import io.isomarcte.twitters.oauth.OAuthSecrets
import io.isomarcte.twitters.oauth.OAuthToken
import io.isomarcte.twitters.oauth.util.SortedMapUtil

/** Tests for the [[Requests]] utilities and constants. */
final class RequestsSpec extends Specification with ScalaCheck {

  // Private Values //

  /** A simple custom Arbitrary instance for Uri. There are several edge cases
    * in the http4s-testing instance which we don't care about for Twitter calls.
    */
  private[this] implicit val arbUri: Arbitrary[Uri] =
    Arbitrary(
      Gen.const(
        Uri.uri("https://developer.twitter.com/en/docs/basics/authentication/guides/authorizing-a-request")
      )
    )

  private[this] implicit val arbMethod: Arbitrary[Method] =
    Arbitrary(genStandardMethod)

  // Property Tests

  /** This property checks that signing a request with valid inputs always
    * yields a valid result with an "Authorization" header set.
    */
  private[this] val signatureProp: Prop =
    Prop.forAll{
      (consumerKey: ConsumerKey,
        oauthToken: OAuthToken,
        oauthSecrets: OAuthSecrets,
        method: Method,
        uri: Uri,
        httpVersion: HttpVersion,
        bodyParameters: SortedMap[String, Option[String]]
      ) =>
      val requestWithBody: RequestWithBody =
        RequestWithBody(
          Request(
            method,
            uri,
            httpVersion,
            body = Stream.chunk(Chunk.bytes(SortedMapUtil.formEncode(bodyParameters).getBytes(UTF_8)))
          ),
          bodyParameters
        )
          (for {
            config <- OAuthConfig.defaultSecureRandom[Task](
              consumerKey,
              oauthToken,
              oauthSecrets)
            signedRequest <- Requests.authorizeRequestWithBody[Task](
              config
            )(requestWithBody)
          } yield
              signedRequest.headers.filter(_.name === Authorization.name).size === 1).unsafeRun
    }

  // Specification //

  /** Specification */
  def is = s2"""

  Specification for Twitters Requests.


  Signing Requests should always yield a request with an "Authorization" header. ${this.signatureProp}
  """
}
