package io.isomarcte.twitters.oauth

// Java Standard Library Imports //

import java.nio.charset.StandardCharsets.UTF_8

// General Imports //

import cats.implicits._

import fs2.Task

import org.apache.commons.codec.net.URLCodec

import org.scalacheck.Prop

import org.specs2.ScalaCheck
import org.specs2.Specification
import org.specs2.matcher.MatchResult

// Local Imports //

import io.isomarcte.twitters.oauth.util.TwitterExample

/** Specification for [[OAuthUtil]]. */
final class OAuthUtilSpec extends Specification with ScalaCheck {

  // Private Values //

  /** An instance of the Apache Commons Codec URLEncoder to use to validate
    * our implementation.
    */
  private[this] val apacheUrlEncoder: URLCodec =
    new URLCodec(UTF_8.toString)

  // Properties

  /** A property which states that the output of the Apache Commons Codec
    * Encoder and our encoder should be the same.
    */
  private[this] val urlEncodingProp: Prop =
    Prop.forAll{(s: String) =>
      val l1: List[Byte] = OAuthUtil.percentEncode(s).getBytes(UTF_8).toList
      val l2: List[Byte] = this.apacheEncode(s).getBytes(UTF_8).toList
      l1 must_== l2
    }

  // Unit Tests


  // The next two tests were created because of a transient property
  // failure. The problem turned about to be that the Apache URL encoder also
  // encodes ' ' as "%20" rather than "%20".

  /** Test that the space character is properly handled. */
  private[this] val spaceEncoding = {
    val input: String = " "

    OAuthUtil.percentEncode(input) must_== this.apacheEncode(input)
  }

  /** An instance of a transient property failure. */
  private[this] val failingTest = {
    val input: String = " 軨咯㴸䟀⒛퉱悎俄䩦廏"
    OAuthUtil.percentEncode(input) must_== this.apacheEncode(input)
  }

  /** Example Take from
    * [[https://developer.twitter.com/en/docs/basics/authentication/guides/creating-a-signature.html]]
    */
  private[this] val twitterExample: MatchResult[Any] = {
    val expectedResult: String = "hCtSmYh+iHYCEqBWrE7C7hYmtUk="
    val computedThunk: Task[OAuthSignature] =
      OAuthUtil.createSignature[Task](
        TwitterExample.signatureExample.exampleSecrets
      )(
        TwitterExample.signatureExample.exampleRequest.method,
        TwitterExample.signatureExample.exampleRequest.url,
        TwitterExample.signatureExample.exampleRequest.allParameters,
      )
    val x = {
      for {
        computedValue <-  computedThunk
      } yield computedValue.signature must_== expectedResult
    }

    x.unsafeRun
  }

  // Private Functions //

  private[this] def apacheEncode(s: String): String =
    this.replacePlus(this.apacheUrlEncoder.encode(s))

  private[this] def replacePlus(s: String): String = s.replaceAll("""\+""", "%20")

  // Specification //

  /** Specification. */
  def is = s2"""

  Specification for the OAuthUtils.

  Property Specifications are defined below.

  The URL percent encoder (RFC 3986, Section 2.1) should yield the same result
  as the Apache Commons Codec URL percent encoder. ${this.urlEncodingProp}

  Unit Specifications are defined below.

  The example signature given in the Twitter API docs should be the same when
  computed. ${this.twitterExample}

  The space String " " should properly encode to "%20". ${this.spaceEncoding}

  The Unicode string " 軨咯㴸䟀⒛퉱悎俄䩦廏" should properly encode. ${this.failingTest}

  """
}
