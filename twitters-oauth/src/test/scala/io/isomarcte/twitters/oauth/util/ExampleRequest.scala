package io.isomarcte.twitters.oauth.util

// Java Standard Library Imports //

import java.net.URL

// Scala Standard Library Imports //

import scala.collection.SortedMap
import scala.collection.SortedSet

// General Imports //

import cats.implicits._

// Local Imports //

import io.isomarcte.twitters.oauth.HTTPMethod
import io.isomarcte.twitters.oauth.OAuthParameters

/** A ''very'' simple record which defines ''part'' of an HTTP
  * request. Specifically the parts used during OAuth. This class should not
  * ever be used for any real HTTP messages. It is merely intended to be a
  * descriptive record for testing purposes.
  *
  * @constructor
  * @param method the [[HTTPMethod]].
  * @param url the URL.
  * @param bodyParameters any parameters which will be in the body of the
  *        request.
  * @param queryParameters any parameters which will be in the query string of
  *        the request.
  * @param oauthParameters the oauth parameters for the request. These will
  *        end up in the value of the "Authorization" header.
  */
final case class ExampleRequest(
  method: HTTPMethod,
  url: URL,
  bodyParameters: SortedMap[String, SortedSet[String]],
  queryParameters: SortedMap[String, SortedSet[String]],
  oauthParameters: OAuthParameters
) {

  // Public Lazy Values //

  /** Merge all the parameters (body, oauth, and query) into a single
    * [[OAuthParameters]] value. This is useful during the signing process as
    * all of these values will be required to generate the "oauth_signature"
    * value.
    */
  lazy val allParameters: OAuthParameters =
    ExampleRequest.allParameters(this)
}

/** Helper functions for working with [[ExampleRequest]] values. */
object ExampleRequest {

  /** Merge all the parameters (body, oauth, and query) into a single
    * [[OAuthParameters]] value. This is useful during the signing process as
    * all of these values will be required to generate the "oauth_signature"
    * value.
    *
    * @param e an [[ExampleRequest]] for which to merge the parameters.
    *
    * @return the merged parameters in terms of an [[OAuthParameters]]
    *         values.
    */
  def allParameters(e: ExampleRequest): OAuthParameters =
    OAuthParameters(
      e.bodyParameters
    ) |+|
    OAuthParameters(
      e.queryParameters
    ) |+|
    e.oauthParameters
}
