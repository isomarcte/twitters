package io.isomarcte.twitters.oauth.util

// Java Standard Library Imports //

import java.net.URL

// Scala Standard Library Imports //

import scala.collection.SortedMap
import scala.collection.SortedSet

// General Imports //

import cats.implicits._

// Local Imports //

import io.isomarcte.twitters.oauth.ConsumerSecret
import io.isomarcte.twitters.oauth.HTTPMethod
import io.isomarcte.twitters.oauth.OAuthConstants
import io.isomarcte.twitters.oauth.OAuthParameters
import io.isomarcte.twitters.oauth.OAuthSecrets
import io.isomarcte.twitters.oauth.OAuthToken
import io.isomarcte.twitters.oauth.OAuthTokenSecret

/** A simple record used to group unit test values taken from known correct
  * requests.
  *
  * @note this record primarily exists because for certain parameters
  *       ([[Nonce]] and [[Timestamp]] come to mind) the standard APIs don't
  *       allow normal creation of these values. This class bypasses these
  *       safety mechanisms to encode known correct values for unit tests.
  *
  * @constructor
  * @param exampleRequest an [[ExampleRequest]].
  * @param exampleSecrets the [[OAuthSecrets]] used in the example.
  */
final case class TwitterExample(
  exampleRequest: ExampleRequest,
  exampleSecrets: OAuthSecrets
)

/** Contains constant instances of example requests and secrets. */
object TwitterExample {

  /** The example request used in the Twitter API signature documentation.
    *
    * @see [[https://developer.twitter.com/en/docs/basics/authentication/guides/creating-a-signature]]
    */
  val signatureExampleRequest: ExampleRequest =
    ExampleRequest(
      HTTPMethod.POST,
      new URL("https://api.twitter.com/1.1/statuses/update.json"),
      SortedMap(
        ("status" ->
         SortedSet("Hello Ladies + Gentlemen, a signed OAuth request!"))
      ),
      SortedMap(("include_entities" -> SortedSet(true.show))),
      OAuthParameters(
        SortedMap(
          (OAuthConstants.oauthConsumerKey -> SortedSet("xvz1evFS4wEEPTGEFPHBog")),
          (OAuthConstants.oauthNonce -> SortedSet("kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg")),
          OAuthConstants.oauthSignaturePair,
          (OAuthConstants.oauthTimestamp -> SortedSet(1318622958.show)),
          OAuthToken("370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb").asPair,
          (OAuthConstants.oauthVersionPair)
        )
      )
    )

  /** The example [[OAuthSecrets]] used in the Twitter API signature documentation.
    *
    * @see [[https://developer.twitter.com/en/docs/basics/authentication/guides/creating-a-signature]]
    */
  val signatureExampleSecrets: OAuthSecrets =
    OAuthSecrets(
      OAuthTokenSecret("LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE"),
      ConsumerSecret("kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw")
    )

  /** The example request and [[OAuthSecrets]] used in the Twitter API signature
    * documentation.
    *
    * @see [[https://developer.twitter.com/en/docs/basics/authentication/guides/creating-a-signature]]
    */
  val signatureExample: TwitterExample =
    TwitterExample(
      this.signatureExampleRequest,
      this.signatureExampleSecrets
    )
}
