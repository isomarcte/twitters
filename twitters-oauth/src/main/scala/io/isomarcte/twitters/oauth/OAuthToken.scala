package io.isomarcte.twitters.oauth

// Scala Standard Library Imports //

import scala.collection.SortedSet

/** A newtype for a OAuthToken values.
  *
  * @constructor
  * @param token the underlying token String.
  */
final case class OAuthToken(token: String) {

  // Public Lazy Values //

  /** Convert this value into a pair suitable for building
    * [[OAuthParameters]].
    */
  lazy val asPair: (String, SortedSet[String]) =
    OAuthToken.asPair(this)
}

/** Functions for working with [[OAuthToken]] values. */
object OAuthToken {

  // Public Functions //

  /** Convert a [[OAuthToken]] into a pair suitable for building
    * [[OAuthParameters]].
    *
    * @param oauthToken the [[OAuthToken]] to convert into a pair.
    *
    * @return a pair suitable for building a [[OAuthParameters]] value.
    */
  def asPair(oauthToken: OAuthToken): (String, SortedSet[String]) =
    OAuthConstants.oauthToken -> SortedSet(oauthToken.token)
}
