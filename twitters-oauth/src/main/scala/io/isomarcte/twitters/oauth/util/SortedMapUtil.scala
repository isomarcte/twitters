package io.isomarcte.twitters.oauth.util

// Scala Standard Library Imports //

import scala.collection.SortedMap

// General Imports //

import cats._
import cats.implicits._

/** Utilities for working with SortedMap values. */
object SortedMapUtil {

  /** Given a SortedMap convert it into a String representing the standard
    * HTML form encoding.
    *
    * @param m the SortedMap to encode.
    *
    * @tparam K the key type of the map. Must have an Show instance.
    * @tparam V the value type of the map. Must have an Show instance.
    * @tparam F a Foldable type for the values.
    *
    * @return a String representing the encoded SortedMap.
    */
  def formEncode[K : Show, V : Show, F[_] : Foldable](
    m: SortedMap[K, F[V]]
  ): String = m.map{
    case (k, vs) => vs.foldMap(v => List(k.show |+| "=" |+| v.show)).mkString("&")
  }.mkString("&")
}
