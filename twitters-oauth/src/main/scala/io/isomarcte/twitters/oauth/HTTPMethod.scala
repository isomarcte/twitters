package io.isomarcte.twitters.oauth

// General Imports //

import cats.Show
import cats.implicits._

/** A simple ADT defining the HTTP methods.
  *
  * @note we could have used the method types defined in http4s, but as this
  *       is the only place in this module where we might have wanted an
  *       http4s type it was decided just to redefine the ADT and avoid the
  *       dependency on the module.
  */
sealed trait HTTPMethod extends Serializable with Product {

  /** Convert any method into a String. */
  final val method: String =
    this.show
}

/** Typeclass instances for [[HTTPMethod]]. */
trait HTTPMethodInstances {
  // Typeclass Instances //

  /** Show instance for [[HTTPMethod]]. */
  implicit val showInstance: Show[HTTPMethod] =
    Show.fromToString
}

/** ADT constructors for [[HTTPMethod]]. */
object HTTPMethod extends HTTPMethodInstances {

  // ADT Constructors //

  final case object ACL extends HTTPMethod
  final case object `BASELINE-CONTROL` extends HTTPMethod
  final case object BIND extends HTTPMethod
  final case object CHECKIN extends HTTPMethod
  final case object CHECKOUT extends HTTPMethod
  final case object CONNECT extends HTTPMethod
  final case object COPY extends HTTPMethod
  final case object DELETE extends HTTPMethod
  final case object GET extends HTTPMethod
  final case object HEAD extends HTTPMethod
  final case object LABEL extends HTTPMethod
  final case object LINK extends HTTPMethod
  final case object LOCK extends HTTPMethod
  final case object MERGE extends HTTPMethod
  final case object MKACTIVITY extends HTTPMethod
  final case object MKCALENDAR extends HTTPMethod
  final case object MKCOL extends HTTPMethod
  final case object MKREDIRECTREF extends HTTPMethod
  final case object MKWORKSPACE extends HTTPMethod
  final case object MOVE extends HTTPMethod
  final case object OPTIONS extends HTTPMethod
  final case object ORDERPATCH extends HTTPMethod
  final case object PATCH extends HTTPMethod
  final case object POST extends HTTPMethod
  final case object PROPFIND extends HTTPMethod
  final case object PROPPATCH extends HTTPMethod
  final case object PUT extends HTTPMethod
  final case object REBIND extends HTTPMethod
  final case object REPORT extends HTTPMethod
  final case object SEARCH extends HTTPMethod
  final case object TRACE extends HTTPMethod
  final case object UNBIND extends HTTPMethod
  final case object UNCHECKOUT extends HTTPMethod
  final case object UNLINK extends HTTPMethod
  final case object UNLOCK extends HTTPMethod
  final case object UPDATE extends HTTPMethod
  final case object UPDATEREDIRECTREF extends HTTPMethod
  final case object `VERSION-CONTROL` extends HTTPMethod
}
