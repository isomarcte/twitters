package io.isomarcte.twitters.oauth

// Scala Language Features //

import scala.language.higherKinds

// Java Standard Library Imports //

import java.net.URL
import java.net.URLEncoder
import java.nio.charset.StandardCharsets.UTF_8

// General Imports //

import cats.implicits._

import fs2.util.Effect
import fs2.util.syntax.FunctorOps

// Local Imports //

import io.isomarcte.twitters.oauth.util.SortedMapUtil

/** Utilities for creating OAuth values.
  *
  * In particular, this module provides tooling fore creating an OAuth
  * signature
  * [[https://developer.twitter.com/en/docs/basics/authentication/guides/creating-a-signature.html]]
  * (because I couldn't find a library that did it in Scala) and an
  * implementation of the RFC 3986 percent encoding (mostly the one from the
  * Java standard library with a modification for handling ' '[space]
  * characters).
  */
object OAuthUtil {

  // Public Functions //

  /** Given the require OAuth secrets and a HTTP method, URL, and all
    * parameters (query, post, and OAuth), create an oauth_signature for the
    * request.
    *
    * @param secrets the [[OAuthSecrets]]. Do not serialize these.
    *
    * @param method an [[HTTPMethod]].
    * @param url the URL for the request. This should ''not'' have a path,
    *        query, or fragment.
    * @param params the [[OAuthParameters]] which will be signed. This should
    *        include the standard OAuth related values, as well as any query
    *        or body parameters.
    *
    * @tparam E an effect type.
    *
    * @return an effect which will yield an [[OAuthSignature]]
    */
  def createSignature[E[_] : Effect](
    secrets: OAuthSecrets
  )(
    method: HTTPMethod,
    url: URL,
    params: OAuthParameters,
  ): E[OAuthSignature] = {
    val messageBytes: Array[Byte] =
      (method.show |+|
       "&" |+|
       this.percentEncode(url.toString) |+|
       "&" |+|
       this.percentEncode(this.encodeParams(params))).getBytes(UTF_8)
    for {
      result <- OAuthSignature.sign(messageBytes, secrets)
    } yield result
  }

  /** Given a String value, percent encode this value in the UTF-8 character
    * set.
    *
    * @param s the String to percent encode.
    *
    * @return a percent encoded string.
    *
    * @see [[https://tools.ietf.org/html/rfc3986#section-2.1]]
    *
    * @note this was written to avoid a dependency in the compile scope on
    *       Apache Commons Codec.
    */
  def percentEncode(
    s: String
  ): String =
    URLEncoder.encode(s, UTF_8.toString).replaceAll("""\+""", "%20")

  // Private Functions //

  /** Encode [[OAuthParameters]] according to the Twitter API doc.
    *
    * @param params the [[OAuthParameters]] to encode.
    *
    * @see [[https://developer.twitter.com/en/docs/basics/authentication/guides/percent-encoding-parameters.html]]
    */
  private[this] def encodeParams(
    params: OAuthParameters
  ): String =
    // Foldable instance for SortedSet is not in cats 0.9.0 so we have to
    // force it to a list.
    SortedMapUtil.formEncode((params.percentEncoded).mapValues(_.toList))
}
