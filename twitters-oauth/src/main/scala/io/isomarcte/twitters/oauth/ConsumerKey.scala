package io.isomarcte.twitters.oauth

// Scala Standard Library Imports //

import scala.collection.SortedSet

// General Imports //

import cats.Show
import cats.implicits._

/** A newtype for a ConsumerKey String.
  *
  * @constructor
  * @param key the consumer key String.
  */
final case class ConsumerKey(key: String) {

  // Public Lazy Values //

  /** Convert this value into a pair suitable for building
    * [[OAuthParameters]].
    */
  lazy val asPair: (String, SortedSet[String]) =
    ConsumerKey.asPair(this)
}

/** Typeclass instances for [[ConsumerKey]]. */
trait ConsumerKeyInstances {

  // Typeclass Instances //

  /** Show instance for [[ConsumerKey]]. */
  implicit val showConsumerKeyInstance: Show[ConsumerKey] =
    Show.show(_.key)
}

/** Functions which operate on [[ConsumerKey]]. */
object ConsumerKey extends ConsumerKeyInstances {

  // Public Functions //

  /** Convert a [[ConsumerKey]] into a pair suitable for building
    * [[OAuthParameters]].
    *
    * @param consumerKey the [[ConsumerKey]] to convert into a pair.
    *
    * @return a pair suitable for building a [[OAuthParameters]] value.
    */
  def asPair(consumerKey: ConsumerKey): (String, SortedSet[String]) =
    OAuthConstants.oauthConsumerKey -> SortedSet(consumerKey.show)
}
