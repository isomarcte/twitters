package io.isomarcte.twitters.oauth

// Scala Language Features //

import scala.language.higherKinds

// Java Standard Library Imports //

import java.security.SecureRandom
import java.util.Base64

// Scala Standard Library Imports //

import scala.collection.SortedSet

// General Imports //

import cats.Show
import cats.implicits._

import fs2.util.syntax.FunctorOps
import fs2.util.Effect

/** A newtype for a Nonce values.
  *
  * [[Nonce]] values are created in special contexts because they have special
  * security requirements. Notably, one should always use a secure
  * source of entropy for the construction of [[Nonce]] values, rather than
  * just a seeded random. Use of a seeded random could allow an attacker to
  * predict the next nonce, thus breaking the security.
  */
sealed trait Nonce extends Serializable with Product {

  // Abstract Methods //

  /** Convert the [[Nonce]] to a String value. */
  def nonce: String

  // Public Lazy Values //

  /** Convert this value into a pair suitable for building
    * [[OAuthParameters]].
    */
  lazy val asPair: (String, SortedSet[String]) = Nonce.asPair(this)
}

/** Typeclass instances for [[Nonce]] values. */
trait NonceInstances {

  implicit val showNonceInstance: Show[Nonce] =
    Show.show(_.nonce)
}

/** Functions for creating [[Nonce]] values. */
object Nonce extends NonceInstances {

  // Private ADT Constructors //

  /** A hidden ADT constructor for a [[Nonce]] value.
    *
    * @note this is hidden because [[Nonce]] values should only be created
    *       using a SecureRandom side-effect.
    *
    * @constructor
    * @param nonce the underlying String value.
    */
  private[this] final case class NonceImpl(
    override final val nonce: String
  ) extends Nonce

  // Public Functions //

  /** Create an Effect to generate a new [[Nonce]] value from a secure entropy
    * source.
    *
    * @note it is perfectly safe to reuse the effect once created for the
    *       creation of more than one [[Nonce]] value. Each invocation will generate
    *       a new value.
    *
    * @param secureRandom a SecureRandom instance to use to generate the
    *        [[Nonce]] value.
    *
    * @tparam E an Effect type.
    *
    * @return an effect which will generate [[Nonce]] values.
    */
  def nonce[E[_] : Effect](
    secureRandom: SecureRandom
  ): E[Nonce] =
    for {
      bytes <- Effect[E].delay{
        val arr: Array[Byte] = new Array[Byte](32)
        secureRandom.nextBytes(arr)
        arr
      }
    } yield NonceImpl(Base64.getEncoder.encodeToString(bytes))

  /** Convert a [[Nonce]] into a pair suitable for building
    * [[OAuthParameters]].
    *
    * @param nonce the [[Nonce]] to convert into a pair.
    *
    * @return a pair suitable for building a [[OAuthParameters]] value.
    */
  def asPair(nonce: Nonce): (String, SortedSet[String]) =
    OAuthConstants.oauthNonce -> SortedSet(nonce.nonce)
}
