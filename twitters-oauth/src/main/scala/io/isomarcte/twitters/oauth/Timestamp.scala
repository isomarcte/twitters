package io.isomarcte.twitters.oauth

// Scala Language Features //

import scala.language.higherKinds

// Java Standard Library Imports //

import java.time.Instant

// Scala Standard Library Imports //

import scala.collection.SortedSet

// General Imports //

import fs2.util.Effect

/** A wrapper type for a OAuth timestamps. */
sealed trait Timestamp extends Serializable with Product {

  // Abstract Methods //

  /** Get the [[Timestamp]] values as an Instant. */
  def timestamp: Instant

  // Public Lazy values //

  /** Convert this value into a pair suitable for building
    * [[OAuthParameters]].
    */
  lazy val asPair: (String, SortedSet[String]) = Timestamp.asPair(this)
}

/** Private ADT constructors and helper functions for working with
  * [[Timestamp]] values.
  */
object Timestamp {

  // Private ADT Constructors //

  /** A private ADT constructor for [[Timestamp]] values.
    *
    * @note we don't want users constructing these directly as that is error
    *       prone. In just about every case we want the construction of this
    *       value to happen as close as possible to the generation of the
    *       OAuth request. Forcing the user to use a delayed effect for this
    *       solves this well.
    *
    * @constructor
    * @param timestamp the underlying Instant value.
    */
  private[this] final case class TimestampImpl(
    override final val timestamp: Instant
  ) extends Timestamp

  // Public Functions //

  /** Create a new [[Timestamp]] effect, which when run will yield a
    * [[Timestamp]] for the moment of evaluation.
    *
    * @tparam E an effect type for the creation of a [[Timestamp]] value.
    *
    * @return an effect which will yield a new [[Timestamp]] for the moment of
    *         evaluation, on each evaluation.
    */
  def now[E[_] : Effect]: E[Timestamp] =
    Effect[E].delay(TimestampImpl(Instant.now))

  /** Convert a [[Timestamp]] into a pair suitable for building
    * [[OAuthParameters]].
    *
    * @param timestamp the [[Timestamp]] to convert into a pair.
    *
    * @return a pair suitable for building a [[OAuthParameters]] value.
    */
  def asPair(timestamp: Timestamp): (String, SortedSet[String]) =
    "oauth_timestamp" -> SortedSet(timestamp.timestamp.getEpochSecond.toString)
}
