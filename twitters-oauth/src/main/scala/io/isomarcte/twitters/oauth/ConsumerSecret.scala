package io.isomarcte.twitters.oauth

/** A newtype for a ConsumerSecret String.
  *
  * @constructor
  * @param key the consumer secret String.
  */
final case class ConsumerSecret(secret: String){

  // Override the toString so that we don't echo out the secret value to logs
  // and what have you.
  override final val toString: String =
    "ConsumerSecret(<REDACTED>)"
}
