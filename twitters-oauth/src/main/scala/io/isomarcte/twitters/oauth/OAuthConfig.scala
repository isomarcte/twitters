package io.isomarcte.twitters.oauth

// Scala Language Features //

import scala.language.higherKinds

// Java Standard Library Imports //

import java.security.SecureRandom

// General Imports //

import cats.implicits._

import fs2.util.syntax.FunctorOps
import fs2.util.Effect

/** Contains all of the configuration values needed create an OAuth request.
  *
  * @constructor
  * @param secureRandom a SecureRandom instance for generate of [[Nonce]]
  *        values.
  * @param consumerKey the [[ConsumerKey]] value which identifies the
  *        application.
  * @param oauthToken the [[OAuthToken]] which identifies the user for which
  *        the application is acting on behalf of.
  * @param oauthSecrets the [[OAuthSecrets]] which contain the secret values
  *        required to authentication.
  */
final case class OAuthConfig(
  secureRandom: SecureRandom,
  consumerKey: ConsumerKey,
  oauthToken: OAuthToken,
  oauthSecrets: OAuthSecrets
)

/** Helper methods for the construction of [[OAuthConfig]] values. */
object OAuthConfig {

  /** Create an [[OAuthConfig]] with a default SecureRandom value.
    *
    * @note the creation of a SecureRandom is modeled as an effect because of
    *       several reason. Contact with the system entropy pool is generally
    *       considered to be a side effect on the system. If the security
    *       properties are not configured correctly on the system it is
    *       possible that the creation will fail.
    *
    * @note although it is probably safe to run this effect more than once,
    *       keep in mind that doing so will always create, not reuse, a
    *       SecureRandom instance.
    *
    * @param consumerKey the [[ConsumerKey]] value which identifies the
    *        application.
    * @param oauthToken the [[OAuthToken]] which identifies the user for which
    *        the application is acting on behalf of.
    * @param oauthSecrets the [[OAuthSecrets]] which contain the secret values
    *        required to authentication.
    *
    * @tparam E an effect type.
    *
    * @return an effect which when evaluated will yield an [[OAuthConfig]].
    */
  def defaultSecureRandom[E[_] : Effect](
    consumerKey: ConsumerKey,
    oauthToken: OAuthToken,
    oauthSecrets: OAuthSecrets
  ): E[OAuthConfig] = for {
    secureRandom <- Effect[E].delay(new SecureRandom())
  } yield OAuthConfig(secureRandom, consumerKey, oauthToken, oauthSecrets)
}
