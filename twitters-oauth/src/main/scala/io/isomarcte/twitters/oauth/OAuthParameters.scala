package io.isomarcte.twitters.oauth

// Scala Language Features //

import scala.language.higherKinds

// Scala Standard Library Imports //

import scala.collection.SortedMap
import scala.collection.SortedSet

// General Imports //

import cats._
import cats.implicits._

import fs2.util.Effect
import fs2.util.syntax.FunctorOps
import fs2.util.syntax.MonadOps

/** A newtype for the SortedMap which backs [[OAuthParameters]].
  *
  * @constructor
  * @param params the SortedMap which backs the [[OAuthParameters]].
  */
final case class OAuthParameters(
  params: SortedMap[String, SortedSet[String]]
) {

  // Public Lazy Values //

  /** A representation of the values encoding according to RFC 3986. */
  lazy val percentEncoded: SortedMap[String, SortedSet[String]] =
    this.params.map{
      case (a, bs) =>
        (OAuthUtil.percentEncode(a), bs.map(OAuthUtil.percentEncode))
    }
}

/** Typeclass instances for [[OAuthParameters]]. */
trait OAuthParametersInstances {

  // Private Typeclass Instances //

  // These instances are in the Cats 1.0.0-RC1 and later, but sadly not in 0.9.0.
  // Makes me miss GeneralizedNewType deriving from GHC.

  /** A SemigroupK instance for SortedSet. */
  private[this] implicit val semigroupKSortedSetInstance: SemigroupK[SortedSet] =
    new SemigroupK[SortedSet] {

      override final def combineK[A](
        a: SortedSet[A],
        b: SortedSet[A]
      ): SortedSet[A] = a | b
    }

  /** A Semigroup instance for SortedSet as required by the Monoid instance for SortedMap. */
  private[this] implicit def semigroupSortedSetInstance[A]: Semigroup[SortedSet[A]] =
    SemigroupK[SortedSet].algebra[A]

  /** A Monoid instance for SortedMap. */
  private[this] implicit def monoidSortedMapInstance[K : kernel.Order, V : Semigroup]: Monoid[SortedMap[K, V]] =
    new Monoid[SortedMap[K, V]] {

      private[this] implicit lazy val ordering: Ordering[K] = kernel.Order[K].toOrdering

      override final def combine(
        a: SortedMap[K, V],
        b: SortedMap[K, V]
      ): SortedMap[K, V] =
        a.foldLeft(b){
          case (acc, (k, v1)) => acc + (k -> acc.get(k).map(v2 => v1 |+| v2).getOrElse(v1))
        }

      override final def empty: SortedMap[K, V] = SortedMap.empty[K, V]
    }

  // Typeclass Instances //

  /** A Monoid instance for [[OAuthParameters]]. */
  implicit val monoidOAuthParametersInstance: Monoid[OAuthParameters] =
    new Monoid[OAuthParameters] {

      override final def combine(
        a: OAuthParameters,
        b: OAuthParameters
      ): OAuthParameters = {
        OAuthParameters(a.params |+| b.params)
      }

      override final def empty: OAuthParameters = OAuthParameters(SortedMap.empty)
    }
}

/** Helper functions for creating [[OAuthParameters]] values. */
object OAuthParameters extends OAuthParametersInstances {

  // Public Functions //

  /** Given an [[OAuthConfig]], create an [[OAuthParameters]] value which
    * contains all the standard OAuth values.
    *
    * @note if there are additional body or query parameters they will need to
    *       be added to this construct.
    *
    * @param config the [[OAuthConfig]] from which to create the
    *        [[OAuthParameters]].
    *
    * @tparam E an effect type to capture the generation of a [[Nonce]] value.
    *
    * @return an effect which when evaluated will yield an [[OAuthParameters]]
    *         with the standard values already populated.
    */
  def fromConfig[E[_] : Effect](
    config: OAuthConfig
  ): E[OAuthParameters] = for {
    nonce <- Nonce.nonce(config.secureRandom)
    timestamp <- Timestamp.now
  } yield {
    OAuthParameters(
      SortedMap(
        config.consumerKey.asPair,
        nonce.asPair,
        OAuthConstants.oauthSignaturePair,
        OAuthConstants.oauthToken -> SortedSet(config.oauthToken.token),
        timestamp.asPair,
        OAuthConstants.oauthVersionPair
    ))
  }
}
