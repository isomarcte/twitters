package io.isomarcte.twitters.oauth

/** Contains the secret values used with OAuth.
  *
  * @note please don't serialize this...
  *
  * @constructor
  * @param oauthTokenSecret the [[OAuthTokenSecret]] value for the user. This
  *        is often obtained through a 3 step OAuth flow, but may be provided
  *        directly by the user in the case of a local authentication (usually
  *        during development).
  * @param consumerSecret the [[ConsumerSecret]] value for the
  *        application.
  */
final case class OAuthSecrets(
  oauthTokenSecret: OAuthTokenSecret,
  consumerSecret: ConsumerSecret
)
