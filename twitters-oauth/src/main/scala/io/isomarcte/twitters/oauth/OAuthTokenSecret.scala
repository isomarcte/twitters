package io.isomarcte.twitters.oauth

/** A newtype for a [[OAuthTokenSecret]] values.
  *
  * @constructor
  * @param secret the underlying String secret.
  */
final case class OAuthTokenSecret(
  secret: String
) {

  // Override the toString so that we don't echo out the secret value to logs
  // and what have you.
  override final val toString: String =
    "OAuthTokenSecret(<REDACTED>)"
}
