package io.isomarcte.twitters.oauth

// Scala Language Features //

import scala.language.higherKinds

// Java Standard Library Imports //

import java.nio.charset.StandardCharsets.UTF_8
import java.util.Base64

// Javax Imports //

import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

// Scala Standard Library Imports //

import scala.collection.SortedSet

// General Imports //

import cats.implicits._

import fs2.util.Effect
import fs2.util.syntax.FunctorOps
import fs2.util.syntax.MonadOps

/** A wrapper for a OAuth signature value. */
sealed trait OAuthSignature extends Serializable with Product {

  // Abstract Methods //

  /** The signature, represented as a String value. */
  def signature: String

  // Public Lazy Values //

  /** Convert this value into a pair suitable for building
    * [[OAuthParameters]].
    */
  lazy val asPair: (String, SortedSet[String]) =
    OAuthSignature.asPair(this)
}

/** Functions for creating [[OAuthSignature]] values. */
object OAuthSignature {

  // Private Values //

  /** The Algorithm name to pass to the Mac and SecretKeySpec constructs. */
  private[this] val HMAC_SHA1: String = "HMACSHA1"

  // Private ADT Constructors //

  /** A hidden ADT constructor for [[OAuthSignature]] values.
    *
    * @note we do not want users creating these values directly as they are
    *       very likely to mess that up.
    *
    * @constructor
    * @param signature the base64 encoded HMAC-SHA1 signature.
    */
  private[this] final case class OAuthSignatureImpl(
    override final val signature: String
  ) extends OAuthSignature

  // Public Functions //

  /** Give an Array of bytes and the secret values, sign the bytes.
    *
    * @param bytes the message bytes to sign.
    * @param secrets the [[OAuthSecrets]] to use to construct the signing key.
    *
    * @tparam E an effect type. The use of Javax cryptographic primitives has
    *         a high probability of a failure with an exception, mostly due to
    *         potential mismatches in the SPI. For this reason working with
    *         them is modeled as effects.
    *
    * @return an effect that when evaluated will yield a [[OAuthSignature]] value.
    */
  def sign[E[_] : Effect](
    bytes: Array[Byte],
    secrets: OAuthSecrets
  ): E[OAuthSignature] = {
    val F: Effect[E] = Effect[E]
    val key: String =
      OAuthUtil.percentEncode(secrets.consumerSecret.secret) |+|
      "&" |+|
      OAuthUtil.percentEncode(secrets.oauthTokenSecret.secret)
    for {
      key <- F.delay(new SecretKeySpec(key.getBytes(UTF_8), this.HMAC_SHA1))
      macInstance <- F.delay(Mac.getInstance(this.HMAC_SHA1))
      mac <- F.delay{
        macInstance.init(key)
        macInstance.doFinal(bytes)}
    } yield OAuthSignatureImpl(Base64.getEncoder.encodeToString(mac))
  }

  /** Convert a [[OAuthSignature]] into a pair suitable for building
    * [[OAuthParameters]].
    *
    * @param signature the [[OAuthSignature]] to convert into a pair.
    *
    * @return a pair suitable for building a [[OAuthParameters]] value.
    */
  def asPair(signature: OAuthSignature): (String, SortedSet[String]) =
    OAuthConstants.oauthSignature -> SortedSet(signature.signature)
}
