package io.isomarcte.twitters.oauth

// Scala Standard Library Imports //

import scala.collection.SortedSet

/** Various miscellaneous constant values used with OAuth. */
object OAuthConstants {

  // Public Values //

  // Strings

  val oauthConsumerKey: String = "oauth_consumer_key"
  val oauthNonce: String = "oauth_nonce"
  val oauthSignature: String = "oauth_signature"
  val oauthSignatureMethod: String = "oauth_signature_method"
  val oauthSignatureMethodValue: String = "HMAC-SHA1"
  val oauthTimestamp: String = "oauth_timestamp"
  val oauthToken: String = "oauth_token"
  val oauthVersion: String = "oauth_version"
  val oauthVersionValue: String = "1.0"

  // Pairs
  val oauthSignaturePair: (String, SortedSet[String]) =
    this.oauthSignatureMethod -> SortedSet(this.oauthSignatureMethodValue)
  val oauthVersionPair: (String, SortedSet[String]) =
    this.oauthVersion -> SortedSet(this.oauthVersionValue)
}
