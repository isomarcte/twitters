# Twitters OAuth Utilities #

This module provides utilities for creating OAuth requests against the Twitter API. It was created because I was unable to find a nice idiomatic Scala library doing the same thing.

_Most_ of this module merely contains newtypes or wrapper types which make it safe to create OAuth values and hide some of the more nasty semantics.

This module has a fairly light dependency footprint, requiring only fs2 and cats in compile scope (modulo transitives).

The primary interface for _using_ this module is `OAuthUtil`.

The ScalaDoc is informative.
